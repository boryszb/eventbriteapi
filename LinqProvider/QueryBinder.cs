﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Linq;


namespace Eventbrite.LinqProvider
{
    public class QueryBinder: Helpers.ExpressionVisitor
    {
        Dictionary<ParameterExpression, Expression> map;

        public QueryBinder()
        {

        }

        public Expression Bind(Expression expression)
        {
            this.map = new Dictionary<ParameterExpression, Expression>();
            return this.Visit(expression);
        }

        protected override Expression VisitMethodCall(MethodCallExpression m)
        {
            if (m.Method.DeclaringType == typeof(Queryable) ||
                m.Method.DeclaringType == typeof(Enumerable))
            {
                switch (m.Method.Name)
                {
                    case "Where":
                        return this.BindWhere(m.Arguments[0], (LambdaExpression)StripQuotes(m.Arguments[1]));
                    case "Select":
                        return this.BindSelect( m.Arguments[0], (LambdaExpression)StripQuotes(m.Arguments[1]));
                    case "OrderBy":
                        return this.BindOrderBy( m.Arguments[0], (LambdaExpression)StripQuotes(m.Arguments[1]), OrderType.Ascending);
                    case "OrderByDescending":
                        return this.BindOrderBy( m.Arguments[0], (LambdaExpression)StripQuotes(m.Arguments[1]), OrderType.Descending);
                }
                throw new NotSupportedException(string.Format("The method '{0}' is not supported", m.Method.Name));
            }
            return base.VisitMethodCall(m);
        }

        protected override Expression VisitParameter(ParameterExpression p)
        {
            //Expression e;
            //if (this.map.TryGetValue(p, out e))
            //{
            //    return e;
            //}
            return p;
        }

        protected override Expression VisitBinary(BinaryExpression b)
        {
           // b.Right.NodeType != ExpressionType.Constant
            //if (b.Right.NodeType == ExpressionType.MemberAccess)
            //{
            //    var right = Convert(b.Right);
            //    b = Expression.Equal(b.Left, right);
            //}
            //// implement logic for caching the binary expression
            //b = base.VisitBinary(b) as BinaryExpression;
            if (map.Count > 0)
            {
                var e = map.Values.First();
                map.Remove(map.Keys.First());
                return Expression.And(b, e);
            }
            return b;
        }

        protected Expression Convert(Expression u)
        {
            LambdaExpression lambda = Expression.Lambda(u);
            Delegate fn = lambda.Compile();
            return Expression.Constant(fn.DynamicInvoke(null), u.Type);
            // return base.VisitConvert(u);
        }
        private Expression BindOrderBy( Expression source, LambdaExpression predicate, OrderType order)
        {
            // get where filter
            QueryExpression query = (QueryExpression)this.Visit(source);

            // get the projection
            OrderExpression orderExpression = this.VisitOrderBy(predicate, order);

            return new QueryExpression(query.Type, query.Query, query.Projector, orderExpression);
        }

        // The nuature of eventbrite api is so limited currently that it doesn't make sens to
        // implement anything more than where and select methods. 
        // Select is assumed to go after where method.
        private Expression BindWhere(Expression source, LambdaExpression predicate)
        {

            var query = (QueryExpression)this.Visit(source);
            Expression where = this.Visit(predicate.Body);
            this.map[predicate.Parameters[0]] = where;
            return new QueryExpression(query.Type, Expression.Lambda(where, map.Keys.First()), query.Projector, query.Order);
        }

        private Expression BindSelect( Expression source, LambdaExpression selector)
        {
            // select goes always after where.

            // get where filter
            QueryExpression query = (QueryExpression)this.Visit(source);

            // get the projection
           // Expression expression = this.Visit(selector);

            return new QueryExpression(query.Type, query.Query, selector, query.Order);
        }

        private OrderExpression VisitOrderBy(LambdaExpression e, OrderType order)
        {
            var member = e.Body as MemberExpression;

            return new OrderExpression(order, member.Member);
        }
        private static Expression StripQuotes(Expression e)
        {
            while (e.NodeType == ExpressionType.Quote)
            {
                e = ((UnaryExpression)e).Operand;
            }
            return e;
        }

        private bool IsQueryObject(object value)
        {
            IQueryable q = value as IQueryable;
            return q != null && q.Expression.NodeType == ExpressionType.Constant;
        }

        private QueryExpression GetQueryExpression(object value)
        {
            IQueryable q = value as IQueryable;
            return new QueryExpression(q?.ElementType, null, null, null);
        }

        protected override Expression VisitConstant(ConstantExpression c)
        {

            if (this.IsQueryObject(c.Value))
            {
                return GetQueryExpression(c.Value);
            }
           
            return c;
        }
    }
}
