﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eventbrite.LinqProvider
{

    public enum OrderType
    {
        Ascending,
        Descending
    }
    public class OrderExpression
    {
        readonly OrderType _orderType;
        readonly MemberInfo _member;
        readonly string reverse;
        public OrderExpression(OrderType orderType, MemberInfo member)
        {
            member = member ?? throw new ArgumentNullException(nameof(member));
            if (member.Name != "ByDate") throw new ArgumentException($"not supported filed {member.Name}");

            this._orderType = orderType;
            this._member = member;
            reverse = _orderType == OrderType.Descending ? "-" : string.Empty;
        }
        public OrderType OrderType => _orderType;
        public MemberInfo Member => _member;

        public override string ToString()
        {

            return $"&sort_by={reverse}{_member.Name}";
        }
    }
}
