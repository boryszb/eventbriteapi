﻿using EventbriteClient;
using EventbriteClient.Adapter;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;

namespace Eventbrite.LinqProvider
{
    public class RequestInfo
    {
        public string UrlRequest { get; set; }
        public LambdaExpression Projector { get; set; }
        public bool IsDefaultSet { get; set; }
        public bool IsCount { get; set; }
        public IEnumerable<T> BuildProjection<T>( IDataAdapter doc)
        {
            var expression = this.Projector ?? throw new ArgumentNullException(nameof(this.Projector));

            dynamic mapper = Activator.CreateInstance(typeof(Mapper<>).MakeGenericType(expression.Parameters[0].Type), doc);
            dynamic events = mapper.Build();
            Delegate compiledExpression = expression.Compile();

            return GetEnumerable(compiledExpression);

            IEnumerable<T> GetEnumerable(Delegate exp)
            {
                foreach (var item in events)
                {
                    yield return (T)exp.DynamicInvoke(item);
                }
            }
            
        }
    }
}
