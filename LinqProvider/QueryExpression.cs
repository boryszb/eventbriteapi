﻿using System;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eventbrite.LinqProvider
{

    public enum EventBriteExpression
    {
        QueryExpressionType = 1000,
        OrderExpressionType
    }
    public class QueryExpression: Expression
    {
        readonly Expression _query;
        readonly Expression _projector;
        readonly OrderExpression _order;
        readonly Type _type;

        public QueryExpression(Type type, Expression query, Expression projector, OrderExpression order)
        {
            _query = query;
            _projector = projector;
            _order = order;
            _type = type;
        }

        public override ExpressionType NodeType => (ExpressionType)EventBriteExpression.QueryExpressionType;
        public override Type Type => _type;
        public Expression Query => _query;
        public Expression Projector => _projector;
        public OrderExpression Order => _order;
    }
}
