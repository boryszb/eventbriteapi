﻿using System;
using System.Collections.Generic;
using System.Linq;
using EventbriteClient;
using EventbriteClient.Util;
using EventbriteClient.Adapter;
using EventbriteClient.Model;

namespace Eventbrite.LinqProvider
{
    public class LinqReader : LinqReaderBase, IDataReader
    {
        public LinqReader(IUrlBuilder builder, IClientSource source) : base(builder, source)
        {}
        public IEnumerable<T> Read<T>(RequestInfo request)
        {
            var requestUrl = this.CreateRequest(APIMethods.EventSearch)
                                .AddQueryString(request.UrlRequest)
                                .Build();

            var doc = this.GetResponse(requestUrl);
            IDataAdapter data;
            try
            {
                data = this.CreateAdapter(doc);
            }
            catch (EventbriteException ex)
            {

                if (request.IsDefaultSet && ex.ErrorType == "Not Found")
                {
                    return null;
                }

                throw;
            }

            if (request.IsCount)
            {
                return GetCountValue<T>(data);

            }
            else if (request.Projector == null)
            {
                return this.CreateMapper<T>(data).Build();
            }
            else
            {
                return request.BuildProjection<T>(data);
            }

        }

        private IEnumerable<T> GetCountValue<T>(IDataAdapter data)
        {
            T result = default;
            var val = data.Root.Elements().SingleOrDefault(e => e.Name == "summary");
            val = val.Elements().First(e => e.Name == "total_items");
            result = GetValFromString<T>(val.Value);
            yield return result;

        }

        private static T GetValFromString<T>(string s)
        {
            if (string.IsNullOrEmpty(s))
                s = "0";
            return (T)Convert.ChangeType(s, typeof(T));
        }
    }
}
