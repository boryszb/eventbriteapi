﻿using System.Collections.Generic;

namespace Eventbrite.LinqProvider
{
    public interface IDataReader
    {
        IEnumerable<T> Read<T>(RequestInfo request);
    }
}
