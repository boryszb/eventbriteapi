﻿using System;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Text;


namespace Eventbrite.LinqProvider
{
    public class QueryTranslator: Helpers.ExpressionVisitor
    {
        private static readonly Dictionary<string, string> _enumLookup = new Dictionary<string, string>() {{"ThisMonth", "this_month"},{"NextMonth","next_month"},
                                                                                                   {"ThisWeek", "this_week"}, {"ThisWeekend", "this_weekend"},
                                                                                                    {"NextWeek", "next_week"}, {"Today", "today" }, {"Tomorrow", "tomorrow"} };
        private readonly Dictionary<string, object> _nameLookup = new Dictionary<string, object>();
        private static readonly Dictionary<string, string> _paramLookup = new Dictionary<string, string>(){{"datemodifiedkeyword", "date_modified.keyword"}, {"date","start_date" }, {"datekeyword", "start_date.keyword" }, {"user", "user.id"}, {"organizer", "organizer.id"}, {"within", "location.within"}, {"longitude", "location.longitude"}, {"latitude","location.latitude"}, {"address","location.address"}, {"city", "ci"}, {"region", "re"}, {"country", "co"}, {"postalcode", "postal_code"},
        {"withinunit", "within_unit"}, {"datemodified", "date_modified"}, {"sinceid", "since_id"}, {"keywords","q"} };

        StringBuilder sb;
        bool containsAddress;
        public QueryTranslator() { }

        public string Translate(QueryExpression expression)
        {
            this.sb = new StringBuilder();
            containsAddress = false;
            _nameLookup.Clear();

            this.Visit(expression.Query);

            return FormatQuery(expression.Order);
        }

        private static Expression StripQuotes(Expression e)
        {

            while (e.NodeType == ExpressionType.Quote)
            {
                e = ((UnaryExpression)e).Operand;
            }

            return e;
        }

        protected override Expression VisitUnary(UnaryExpression u)
        {

            switch (u.NodeType)
            {

                case ExpressionType.Not:
                    sb.Append(" NOT ");
                    this.Visit(u.Operand);
                    break;
                default:
                    throw new NotSupportedException(string.Format("The unary operator '{0}' is not supported", u.NodeType));
            }

            return u;

        }

        protected override Expression VisitBinary(BinaryExpression b)
        {

            //sb.Append("(");
            this.Visit(b.Left);

            switch (b.NodeType)
            {

                case ExpressionType.And:
                    sb.Append("&");
                    break;
                case ExpressionType.Or:
                    sb.Append(" OR");
                    break;
                case ExpressionType.Equal:
                    sb.Append("=");
                    break;
                case ExpressionType.NotEqual:
                    sb.Append(" <> ");
                    break;
                case ExpressionType.LessThan:
                    sb.Append(" < ");
                    break;
                case ExpressionType.LessThanOrEqual:
                    sb.Append(" <= ");
                    break;
                case ExpressionType.GreaterThan:
                    sb.Append(" > ");
                    break;
                case ExpressionType.GreaterThanOrEqual:
                    sb.Append(" >= ");
                    break;
                default:
                    throw new NotSupportedException(string.Format("The binary operator '{0}' is not supported", b.NodeType));
            }

            this.Visit(b.Right);

            return b;
        }

        protected override Expression VisitConstant(ConstantExpression c)
        {
            switch (Type.GetTypeCode(c.Value.GetType()))
            {
                case TypeCode.String:
                    sb.Append(c.Value);
                    break;
                case TypeCode.Int32:
                    if (EnumType != null && EnumType != typeof(EventbriteClient.Model.Categories))
                    {
                        sb.Append(GetEnumName(c.Value));
                    }
                    else
                    {
                        sb.Append(c.Value);
                    }
                    EnumType = null;
                    break;
                default:
                    sb.Append(c.Value);
                    break;
            }
            return c;
        }

        protected override Expression VisitMemberAccess(MemberExpression m)
        {
            var request = m.Expression as MemberExpression;
            if (request == null || request.Member.Name == "Request")
            {
                if (m.Type.BaseType == typeof(Enum))
                    EnumType = m.Type;

                var paramName = m.Member.Name.ToLower();

                if (paramName == "city" || paramName == "country" || paramName == "region")
                {
                    containsAddress = true;
                }

                if (_paramLookup.ContainsKey(paramName))
                    paramName = _paramLookup[paramName];

                if (paramName != "start_date" && paramName != "date_created" && paramName != "date_modified")
                {
                    if (_nameLookup.ContainsKey(paramName))
                        throw new InvalidOperationException(String.Format("Request url can have only one {0} param", paramName));
                    else
                        _nameLookup.Add(paramName, null);
                }

                sb.Append(paramName);
                return m;
            }

            throw new NotSupportedException(string.Format("The member '{0}' is not supported", m.Member.Name));
        }

        private string GetEnumName(object val)
        {
            var name = Enum.GetName(EnumType, val);
            //if (name == null)
            //    return ProcessFlags(val);
            //if (_formats.ContainsKey(name))
            //    return _formats[name].ToString();
            if (_enumLookup.ContainsKey(name))
                return _enumLookup[name];
            return name;
        }

        private string FormatQuery(OrderExpression order)
        {
            if (containsAddress)
            {

                // we have to compound the address params 
                StringBuilder query = new StringBuilder(80);

                // replace queryParams with span?
                var queryParams = sb.ToString().Split('&');
                string[] position = new string[3];
                for (int i = 0; i < queryParams.Length; i++)
                {
                    if (queryParams[i].StartsWith("ci="))
                    {
                        position[0] = queryParams[i].Substring(3);
                    }
                    else if (queryParams[i].StartsWith("re="))
                    {
                        position[1] = queryParams[i].Substring(3);
                    }
                    else if (queryParams[i].StartsWith("co="))
                    {
                        position[2] = queryParams[i].Substring(3);
                    }
                    else
                    {
                        query.Append(queryParams[i]);
                        query.Append('&');
                    }
                }

                query.Append("location.address=");
                bool addComa = false;
                for (int i = 0; i < position.Length; i++)
                {
                    if (position[i] == null) continue;

                    if (addComa)
                    {
                        query.Append(',');
                    }
                    else
                    {
                        addComa = true;
                    }

                    query.Append(position[i]);

                }
                sb = query;
            }
            if (order != null)
            {
                sb.Append(order);
            }
            return sb.ToString();
        }
        private Type EnumType { get; set; }
    }
}
