﻿using System;
using EventbriteClient;
using EventbriteClient.Factory;
using EventbriteClient.Model;

namespace Eventbrite.LinqProvider
{
    public class EventbriteContext
    {
        readonly IDataReader _reader;
        EventbriteQuery<Event> _query;

        public EventbriteContext(string appKey)
        {
            var source = new EventbriteFactory<IClientSource>().GetInstance();
            var builder = new EventbriteFactory<IUrlBuilder>().GetInstance();

            source.SetKey(appKey);
            _reader = new LinqReader(builder, source);
        }

        public EventbriteContext(IUrlBuilder builder, IClientSource source)
        {
            _reader = new LinqReader(builder, source);
        }

        public EventbriteQuery<Event> Query
        {
            get
            {
                if (_query == null)
                {
                    _query = new EventbriteQuery<Event>(new  EventbriteQueryProvider(_reader));
                }
                return _query;
            }
        }
    }
}
