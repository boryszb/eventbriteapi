﻿using System;
using System.Reflection;
using System.Linq.Expressions;

namespace Eventbrite.LinqProvider
{
    public class EventbriteQueryProvider: QueryProvider
    {
        readonly IDataReader _reader;
        readonly MethodInfo _readMethod;
        public EventbriteQueryProvider(IDataReader reader)
        {
            _reader = reader ?? throw new ArgumentNullException("reader");
            _readMethod = _reader.GetType().GetMethod("Read");
        }

        public override string GetQueryText(Expression expression)
        {
            return this.Translate(expression).UrlRequest;
        }
        public override object Execute(Expression expression)
        {
            var result = this.Translate(expression);
            Type elementType = Helpers.TypeSystem.GetElementType(expression.Type);
            return _readMethod.MakeGenericMethod(elementType).Invoke(_reader, new[]{ result });
            
        }

        private RequestInfo Translate(Expression expression)
        {
            expression = Helpers.Evaluator.PartialEval(expression);
            QueryExpression exp = new QueryBinder().Bind(expression) as QueryExpression;
            string queryString = new QueryTranslator().Translate(exp);

            return new RequestInfo { UrlRequest = queryString, Projector = exp.Projector as LambdaExpression };
        }
    }
}
