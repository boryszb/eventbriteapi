﻿using EventbriteClient.Adapter;
using EventbriteClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EventbriteClient.Model;

namespace Eventbrite.LinqProvider
{
    public abstract class LinqReaderBase
    {
        readonly IUrlBuilder _urlBuilder;
        readonly IClientSource _source;

        protected LinqReaderBase(IUrlBuilder builder, IClientSource source)
        {
            _urlBuilder = builder;
            _source = source;
        }

        public virtual string GetResponse(Uri requestUrl)
        {
            string doc = _source.Read(requestUrl);

            return doc;
        }
        protected virtual IDataAdapter CreateAdapter(string doc)
        {
            var data = new JsonAdapter(doc);
            if (data.IsErrorMessage())
            {
                var errorMessage = data.Root.Elements().First(e => e.Name == "error_description").Value;
                var errorType = data.Root.Elements().First(e => e.Name == "status_code").Value;
                throw new EventbriteException(errorMessage, errorType);
            }
            return data;
        }

        protected virtual IUrlBuilder CreateRequest(string command)
        {

            return _urlBuilder.AddRequestBaseUrl()
                               .AddRequestMethod(command);
        }

        protected virtual IMapper<T> CreateMapper<T>(IDataAdapter data)
        {
            return new Mapper<T>(data);
        }


    }
}
