﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Eventbrite.LinqProvider
{
   
    // evaluates queries against Eventbrite api
    public class EventbriteQuery<T> : IQueryable<T>, IQueryable, IEnumerable<T>, IEnumerable, IOrderedQueryable<T>, IOrderedQueryable
    {
        // translates the expression tree into 
        // Eventbrite query 
        readonly QueryProvider _provider;
       
        // expression tree representing linq query
        readonly Expression _expression;

        public EventbriteQuery(QueryProvider provider)
        {
            _provider = provider;
            _expression = Expression.Constant(this);

        }

        public EventbriteQuery(QueryProvider provider, Expression expression)
        {
            _provider = provider;
            _expression = expression;
        }

        // executes the expression tree associated with an IQuarable
        // the execution of the expression tree is implemented by
        // associated query provider. 
        public IEnumerator<T> GetEnumerator()
        {
            return (this._provider.Execute(this._expression) as IEnumerable<T>).GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return ((System.Collections.IEnumerable)this._provider.Execute(this._expression)).GetEnumerator();
        }


        public Type ElementType
        {
            get { return typeof(T); }
        }

        public System.Linq.Expressions.Expression Expression
        {
            get { return _expression; }
        }

        public IQueryProvider Provider
        {
            get { return _provider; }
        }

        public override string ToString()
        {
            return this._provider.GetQueryText(this._expression);
        }
    }
}
