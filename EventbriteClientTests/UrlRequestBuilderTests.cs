﻿
using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EventbriteClient;
using EventbriteClient.Model;
using EventbriteClient.Util;
using EventbriteClient.Factory;
using EventbriteClient.Adapter;

namespace EventbriteClientTests
{
    /// <summary>
    /// Summary description for UrlRequestBuilder
    /// </summary>
    [TestClass]
    public class UrlRequestBuilderTests
    {
        public UrlRequestBuilderTests()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        private IUrlBuilder CreateUrlBuilder()
        {
            return new UrlRequestBuilder();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Build_DefaultSetting_ThrowsArgumentNullException()
        {
            var builder = CreateUrlBuilder();
            var path = builder.Build();
        }

        [TestMethod]
        public void Build_DefaultBaseUrl_ReturnsApiDefaultBaseAddress()
        {
            var builder = CreateUrlBuilder();
            var uri = builder.AddRequestBaseUrl()
                        .Build();

            Assert.IsNotNull(uri);
            Assert.IsTrue(uri.ToString() == "https://www.eventbriteapi.com/v3//");

        }

        [TestMethod]
        public void Build_BaseUrlAdded_ReturnsApiBaseAddress()
        {
            var builder = CreateUrlBuilder();
            var uri = builder.AddRequestBaseUrl("http://www.testapi.com")
                        .Build();

            Assert.IsNotNull(uri);
            Assert.IsTrue(uri.ToString() == "http://www.testapi.com/v3//");

        }

        [TestMethod]
        public void Build_EmptyRequestMethodAdded_ReturnsApiBaseAddress()
        {
            var builder = CreateUrlBuilder();
            var uri = builder.AddRequestBaseUrl("http://www.testapi.com")
                        .AddRequestMethod("")
                        .Build();

            Assert.IsNotNull(uri);
            Assert.IsTrue(uri.ToString() == "http://www.testapi.com/v3//");

        }

        [TestMethod]
        public void Build_EventsSearchRequestAdded_ReturnsEventsSearchApiAddress()
        {
            var builder = CreateUrlBuilder();
            var uri = builder.AddRequestBaseUrl("http://www.testapi.com")
                        .AddRequestMethod("events/search")
                        .Build();

            Assert.IsNotNull(uri);
            Assert.IsTrue(uri.ToString() == "http://www.testapi.com/v3/events/search/");

        }

        [TestMethod]
        public void Build_QueryStringAdded_ReturnsHostPathAndQuery()
        {
            var builder = CreateUrlBuilder();
            var uri = builder.AddRequestBaseUrl("http://www.testapi.com")
                        .AddRequestMethod("events/search")
                        .AddQueryString("city=xxxx")
                        .Build();

            Assert.IsNotNull(uri);
            Assert.IsTrue(uri.ToString() == "http://www.testapi.com/v3/events/search/?city=xxxx");

        }

        [TestMethod]
        public void Build_SetQueryStringWithIndexer_ReturnsHostPathQuery()
        {
            var builder = CreateUrlBuilder();
            builder["city"] = "xxxx";
            builder["country"] = "ccccc";
            builder["region"] = "rr";
            var uri = builder.AddRequestBaseUrl("http://www.testapi.com")
                        .AddRequestMethod("events/search")
                        .Build();

            Assert.IsNotNull(uri);
            Assert.IsTrue(uri.ToString() == "http://www.testapi.com/v3/events/search/?city=xxxx&country=ccccc&region=rr");
        }
        [TestMethod]
        public void Build_OverrideQueryStringIndexerWithAddQueryString_ReturnsHostPathAndAddedQuery()
        {
            var builder = CreateUrlBuilder();
            builder["city"] = "xxxx";
            builder["country"] = "ccccc";
            builder["region"] = "rr";
            var uri = builder.AddRequestBaseUrl("http://www.testapi.com")
                        .AddRequestMethod("events/search")
                        .AddQueryString("page=2")
                        .Build();

            Assert.IsNotNull(uri);
            Assert.IsTrue(uri.ToString() == "http://www.testapi.com/v3/events/search/?page=2");

        }

        [TestMethod]
        public void Build_RequestMethodAdded_ReturnsRequestMethodApiAddress()
        {
            var testCases = new List<(string request, string expected)>();
            testCases.Add((request: "events/{0}/attendees", expected: "http://www.testapi.com/v3/events/{0}/attendees/"));
            testCases.Add((request: "venues/{0}", expected: "http://www.testapi.com/v3/venues/{0}/"));
           
            foreach(var testCase in testCases)
            {
                var builder = CreateUrlBuilder();
                var uri = builder.AddRequestBaseUrl("http://www.testapi.com")
                        .AddRequestMethod(testCase.request)
                        .Build();
                
                Assert.IsTrue(uri.ToString() == testCase.expected);
            }
        }

        [TestMethod]
        public void Build_WithExpend_ReturnsHostPathAndExpandParameters()
        {
            var builder = CreateUrlBuilder();
            var uri = builder.AddRequestBaseUrl("http://www.testapi.com")
                    .AddRequestMethod("events/search")
                    .Expand()
                    .Build();

            Assert.IsTrue(uri.ToString() == "http://www.testapi.com/v3/events/search/?&expand=organizer,venue,category,ticket_classes");
        }

        [TestMethod]
        public void Build_WithExpendFalse_ReturnsHostPathAndExpandParametersWithoutTicketClass()
        {
            var builder = CreateUrlBuilder();
            var uri = builder.AddRequestBaseUrl("http://www.testapi.com")
                    .AddRequestMethod("events/search")
                    .Expand(false)
                    .Build();

            Assert.IsTrue(uri.ToString() == "http://www.testapi.com/v3/events/search/?&expand=organizer,venue,category");
        }
    }
}
