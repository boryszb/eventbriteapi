﻿using System;
using System.Text;
using System.Collections.Generic;
using EventbriteClient;
using EventbriteClient.Model;
using EventbriteClient.Util;
using EventbriteClient.Factory;
using EventbriteClient.Adapter;

using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections;

namespace EventbriteClientTests
{
    /// <summary>
    /// Summary description for JsonReaderTests
    /// </summary>
    [TestClass]
    public class JsonReaderTests
    {
        public JsonReaderTests()
        {            
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void Parse_EmptyObject_ReturnsRootWithNullValue()
        {
            var parser = new JsonReader(JsonInput.EmptyObject);

            var result = parser.Parse() as Dictionary<string, object>;
            
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(Dictionary<string, object>));
            Assert.IsTrue(result.Count == 1);

            Assert.IsNull(result["jobject"]);
        }

        [TestMethod]
        public void Parse_EmptyArray_ReturnsEmptyList()
        {
            var parser = new JsonReader(JsonInput.EmptyArray);

            var result = parser.Parse() as ArrayList;

            Assert.IsNotNull(result);
            Assert.IsTrue(result.Count == 0);
        }

        [TestMethod]
        public void Parse_ArrayWithEmptyObjects_ReturnsListWithEmptyObjects()
        {
            var parser = new JsonReader(JsonInput.ArrayOfEmptyObjects);

            var result = parser.Parse() as ArrayList;
            var object1 = result[0] as Dictionary<string, object>;
            var object2 = result[1] as Dictionary<string, object>;

            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ArrayList));
            Assert.IsTrue(result.Count == 2);
            Assert.IsInstanceOfType(object1, typeof(Dictionary<string, object>));
            Assert.IsInstanceOfType(object2, typeof(Dictionary<string, object>));
            Assert.IsTrue(object1["jobject"] == null);
            Assert.IsTrue(object2["jobject"] == null);

        }

        [TestMethod]
        public void Parse_KeyWithString_ReturnsKeyValue()
        {
            var parser = new JsonReader(JsonInput.KeyString);
            var result = parser.Parse() as Dictionary<string, object>;
            result = result["jobject"] as Dictionary<string, object>;

            Assert.IsTrue(result.ContainsKey("name"));
            Assert.IsTrue(((string) result["name"]) == "Alice");

        }

        [TestMethod]
        public void Parse_KeyWithNum_ReturnsKeyValue()
        {
            var parser = new JsonReader(JsonInput.KeyNum);
            var result = parser.Parse() as Dictionary<string, object>;
            result = result["jobject"] as Dictionary<string, object>;

            Assert.IsTrue(result.ContainsKey("age"));
            Assert.IsTrue(((long)result["age"]) == 35);

        }

        [TestMethod]
        public void Parse_KeyWithNull_ReturnsKeyValue()
        {
            var parser = new JsonReader(JsonInput.KeyNull);
            var result = parser.Parse() as Dictionary<string, object>;
            result = result["jobject"] as Dictionary<string, object>;

            Assert.IsTrue(result.ContainsKey("key"));
            Assert.IsNull(result["key"]);
        }

        [TestMethod]
        public void Parse_KeyWithTrue_ReturnsKeyValue()
        {
            var parser = new JsonReader(JsonInput.KeyTrue);
            var result = parser.Parse() as Dictionary<string, object>;
            result = result["jobject"] as Dictionary<string, object>;

            Assert.IsTrue(result.ContainsKey("key"));
            Assert.IsTrue((bool)result["key"]);
        }

        [TestMethod]
        public void Parse_KeyWithFalse_ReturnsKeyValue()
        {
            var parser = new JsonReader(JsonInput.KeyFalse);
            var result = parser.Parse() as Dictionary<string, object>;
            result = result["jobject"] as Dictionary<string, object>;

            Assert.IsTrue(result.ContainsKey("key"));
            Assert.IsFalse((bool)result["key"]);
        }

        [TestMethod]
        public void Parse_KeyWithObject_ReturnsKeyValue()
        {
            var parser = new JsonReader(JsonInput.KeyObject);
            var result = parser.Parse() as Dictionary<string, object>;
            result = result["jobject"] as Dictionary<string, object>;
          
            Assert.IsTrue(result.ContainsKey("parent"));
            var value = result["parent"] as Dictionary<string, object>;
            Assert.IsNotNull(value);
            value = value["jobject"] as Dictionary<string, object>;
            Assert.IsNotNull(value);
            Assert.IsTrue(value.ContainsKey("child"));
            Assert.IsTrue((string)value["child"] == "val");
        }

        [TestMethod]
        public void Parse_KeyWithArray_ReturnsKeyValue()
        {
            var parser = new JsonReader(JsonInput.KeyArray);
            var result = parser.Parse() as Dictionary<string, object>;
            result = result["jobject"] as Dictionary<string, object>;

            Assert.IsTrue(result.ContainsKey("friends"));
            var value = result["friends"] as ArrayList;
            Assert.IsNotNull(value);
            Assert.IsTrue((string)value[0] == "bolo");
            Assert.IsTrue((string)value[1] == "dodo");
        }

        [TestMethod]
        public void Parse_KeyWithArrayWithDifferentTypes_ReturnsKeyValue()
        {
            var parser = new JsonReader(JsonInput.KeyArrayWithDifferentTypes);
            var result = parser.Parse() as Dictionary<string, object>;
            result = result["jobject"] as Dictionary<string, object>;

            Assert.IsTrue(result.ContainsKey("key"));
            var value = result["key"] as ArrayList;
            Assert.IsNotNull(value);
            Assert.IsTrue((string)value[0] == "text");
            Assert.IsTrue((long)value[1] == 5);
            Assert.IsNull(value[2]);
            Assert.IsInstanceOfType(value[3], typeof(Dictionary<string, object>));
        }

        [TestMethod]
        public void Parse_KeyHexString_ReturnsKeyWithHexString()
        {
            var parser = new JsonReader(JsonInput.KeyHexString);
            var result = parser.Parse() as Dictionary<string, object>;
            result = result["jobject"] as Dictionary<string, object>;

            Assert.IsTrue(result.ContainsKey("num"));
            Assert.IsTrue((string)result["num"] == "0x4af");
        }

        [TestMethod]
        public void Parse_KeyWithUnicode_ReturnsKeyWithString()
        {
            var parser = new JsonReader(JsonInput.KeyUnicode);
            var result = parser.Parse() as Dictionary<string, object>;
            result = result["jobject"] as Dictionary<string, object>;

            Assert.IsTrue(result.ContainsKey("title"));
            Assert.IsTrue((string)result["title"] == "Полтора Землекопа");
        }

        [TestMethod]
        public void Parse_StringWithQuotation_ReturnsKeyWithString()
        {
            var parser = new JsonReader(JsonInput.StringWithQuotation);
            var result = parser.Parse() as Dictionary<string, object>;
            result = result["jobject"] as Dictionary<string, object>;

            Assert.IsTrue(result.ContainsKey("text"));
            Assert.IsTrue((string)result["text"] == "this is quote: \"bla lala  \" in text");
        }

        [TestMethod]
        public void Parse_KeyWithExponent_ReturnsKeyWithDoubleValue()
        {
            var parser = new JsonReader(JsonInput.KeyWithExponent);
            var result = parser.Parse() as Dictionary<string, object>;
            result = result["jobject"] as Dictionary<string, object>;

            Assert.IsTrue(result.ContainsKey("min"));
            Assert.IsTrue((double)result["min"] == -1E+28);
            Assert.IsTrue(result.ContainsKey("max"));
            Assert.IsTrue((double)result["max"] == 1E+28);
        }
    }
}
