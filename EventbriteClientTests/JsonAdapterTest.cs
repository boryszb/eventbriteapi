﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EventbriteClient;
using EventbriteClient.Model;
using EventbriteClient.Util;
using EventbriteClient.Factory;
using EventbriteClient.Adapter;

namespace EventbriteClientTests
{
    [TestClass]
    public class JsonAdapterTest
    {
        [TestMethod]
        public void Adapter_EmptyObject_ReturnsRootWithoutElements()
        {
            var adapter = new JsonAdapter(JsonInput.EmptyObject);
            var root = adapter.Root;

            Assert.IsNotNull(root);
            Assert.IsTrue(root.Name == "jobject");
            Assert.IsTrue(root.Elements().Count() == 0);
           
        }

        [TestMethod]
        public void Adapter_EmptyArray_ReturnsNull()
        {
            var adapter = new JsonAdapter(JsonInput.EmptyArray);
            var root = adapter.Root;

            Assert.IsNull(root);
        }

        [TestMethod]
        public void Adapter_KeyWithString_ReturnsItem()
        {
            var adapter = new JsonAdapter(JsonInput.KeyString);
            var item = adapter.Root.Elements().Single();

            Assert.IsTrue(item.Name == "name");
            Assert.IsTrue(item.Value == "Alice");

        }

        [TestMethod]
        public void Adapter_KeyWithNum_ReturnsItem()
        {
            var adapter = new JsonAdapter(JsonInput.KeyNum);
            var item = adapter.Root.Elements().Single();

            Assert.IsTrue(item.Name == "age");
            Assert.IsTrue(item.Value == "35");

        }

        [TestMethod]
        public void Adapter_KeyWithNull_ReturnsItem()
        {
            var adapter = new JsonAdapter(JsonInput.KeyNull);
            var item = adapter.Root.Elements().Single();

            Assert.IsTrue(item.Name == "key");
            Assert.IsTrue(item.Value == null);
        }

        [TestMethod]
        public void Adapter_KeyWithTrue_ReturnsItem()
        {
           
            var adapter = new JsonAdapter(JsonInput.KeyTrue);
            var item = adapter.Root.Elements().Single();

            Assert.IsTrue(item.Name == "key");
            Assert.IsTrue(item.Value == "True");
        }

        [TestMethod]
        public void Adapter_KeyWithFalse_ReturnsItem()
        {

            var adapter = new JsonAdapter(JsonInput.KeyFalse);
            var item = adapter.Root.Elements().Single();

            Assert.IsTrue(item.Name == "key");
            Assert.IsTrue(item.Value == "False");
        }

        [TestMethod]
        public void Adapter_KeyWithObject_ReturnsNestedItem()
        {
            var adapter = new JsonAdapter(JsonInput.KeyObject);
            var item = adapter.Root.Elements().Single();

            Assert.IsTrue(item.Name == "parent");
            Assert.IsNotNull(item.Elements().Single().Elements().SingleOrDefault(i => i.Name == "child" && i.Value == "val"));
        }

        [TestMethod]
        public void Adapter_KeyWithArray_ReturnsItem()
        {
            
            var adapter = new JsonAdapter(JsonInput.KeyArray);
            var item = adapter.Root.Elements().Single();

            Assert.IsTrue(item.Name == "friends");
            Assert.IsTrue(item.Elements().Count() == 2);
            Assert.IsNotNull(item.Elements().SingleOrDefault(i => i.Value == "bolo"));
            Assert.IsNotNull(item.Elements().SingleOrDefault(i => i.Value == "dodo"));
        }

        [TestMethod]
        public void Adapter_KeyWithArrayWithDifferentTypes_ReturnsItem()
        {
            var adapter = new JsonAdapter(JsonInput.KeyArrayWithDifferentTypes);
            var item = adapter.Root.Elements().Single();

            Assert.IsTrue(item.Name == "key");
            Assert.IsTrue(item.Elements().Count() == 3);
            Assert.IsNotNull(item.Elements().SingleOrDefault(i => i.Value == "text"));
            Assert.IsNotNull(item.Elements().SingleOrDefault(i => i.Value == "5"));
            Assert.IsNotNull(item.Elements().SingleOrDefault(i => i.Value == null));
        }
    }
}
