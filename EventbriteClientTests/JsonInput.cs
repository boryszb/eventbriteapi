﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventbriteClientTests
{
    static class JsonInput
    {
        public const string EmptyObject = @"{}";
        public const string EmptyArray = @"[]";
        public const string ArrayOfEmptyObjects = @"[{},{}]";
        public const string KeyString = "{ \"name\": \"Alice\" }";
        public const string KeyNum = "{ \"age\": 35 } ";
        public const string KeyDouble = "{ \"key\": 34.55 }";
        public const string KeyNull = "{ \"key\": null }";
        public const string KeyTrue = "{ \"key\": true }";
        public const string KeyFalse = "{ \"key\": false }";
        public const string KeyHexString = "{ \"num\":\"0x4af\" }";
        public const string KeyUnicode = "{ \"title\":\"\u041f\u043e\u043b\u0442\u043e\u0440\u0430 \u0417\u0435\u043c\u043b\u0435\u043a\u043e\u043f\u0430\" }";
        public const string KeyObject = "{ \"parent\": {\"child\": \"val\"} }";
        public const string KeyArray = "{ \"friends\": [\"bolo\", \"dodo\"] }";
        public const string KeyArrayWithDifferentTypes = "{\"key\": [\"text\", 5, null, { \"child\": \"val\"}] }";
        public const string StringWithQuotation = "{ \"text\": \"this is quote: \\\"bla lala  \\\" in text\"  }";
        public const string KeyWithExponent = "{\"min\": -1.0e+28, \"max\": 1.0e+28 }";
    }
}
