﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Globalization", "CA1303:Do not pass literals as localized parameters", Justification = "<Pending>", Scope = "member", Target = "~M:EventbriteClient.Adapter.JsonAdapter.Item.#ctor(System.String,System.Collections.ArrayList)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1054:Uri parameters should not be strings", Justification = "<Pending>", Scope = "member", Target = "~M:EventbriteClient.UrlRequestBuilder.AddRequestBaseUrl(System.String)~EventbriteClient.IUrlBuilder")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Globalization", "CA1303:Do not pass literals as localized parameters", Justification = "<Pending>", Scope = "member", Target = "~M:EventbriteClient.UrlRequestBuilder.Build~System.Uri")]

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Build", "CA1812:WebSource is an internal class that is apparently never instantiated. If so, remove the code from the assembly. If this class is intended to contain only static members, make it static (Shared in Visual Basic).", Justification = "<Pending>", Scope = "type", Target = "~T:EventbriteClient.WebSource")]