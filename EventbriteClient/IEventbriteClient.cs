﻿using System;
using System.IO;
using System.Collections.Generic;
using EventbriteClient.Model;
using EventbriteClient.Adapter;
using System.Threading.Tasks;

namespace EventbriteClient
{
    // contract for requesting data from eventbrite
    public interface IEventbriteClient
    {
        // generic request for data  
        string GetResponse(Uri requestUrl);

        Task<String> GetResponseAsync(Uri requestUrl);

        // request for info about attendees of specific event
        IEnumerable<Attendee> EventAttendees(long eventId);

        // request for an event specified by its id
        Event GetEvent(long eventId);

        // request for a venue specified by its id
        Venue GetVenue(int venueId);

        // request for information about current user
        User GetUser();

        // request events of current user
        IEnumerable<Event> GetUserEvents();

        // request information about the orders of a user specified with user id
        IEnumerable<Order> GetUserOrders(long? userId);

        // request orders of an event specified by event id
        IEnumerable<Order> GetEventOrders(long eventId);

        // request information about the categories of events
        IEnumerable<Category> GetCategories();

        // request information about a category
        Category GetCategory(int id);

        // request information about formates
        IEnumerable<Format> GetFormats();

        // request order with specified id
        Order GetOrder(long id);

        // set/get text writer to enter records to the log
        TextWriter LogResult { get; set; }

        // create data adapter for eventbrite data 
        IDataAdapter CreateAdapter(string doc);

        // get media object that represents
        // an image included with Event listing
        Logo GetMediaObject(long id);

        // add information about tickets to the event object 
        bool AddTickets { get; set; }

        // get events by organization id
        IEnumerable<Event> GetEventsByOrganization(long id);

        // get venues by organization id
        IEnumerable<Venue> GetVenuesByOrganization(long orgId);

        // get events by venue id
        IEnumerable<Event> GetEventsByVenue(long id);

        // get attendees of an organization's events
        IEnumerable<Attendee> GetAttendeesByOrganization(long id);

        // get events by series id 
        IEnumerable<Event> GetEventsBySeries(long id);

        // get full html description for an event 
        EventDescription GetEventDescription(long id);

        // get ticket buyer settings by event id
        TicketBuyerSettings GetTicketBuyerSettings(long id);

        // get ticket by id
        Ticket GetTicketBytTicketId(long eventId, long ticketId);

        // get organizations by user id
        IEnumerable<Organization> GetUserOrganizations(long? id);
    }
}
