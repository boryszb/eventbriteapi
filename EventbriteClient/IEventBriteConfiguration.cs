﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventbriteClient
{
    // contract for configuring connection with eventbrite endpoint
    public interface IEventBriteConfiguration
    {
        string UserKey { get; set; }
        string AppKey { get; set; }
        // this two members should belong
        // only to RequestBuilder
        // string RequestURL { get; set; }
        // string MessageFormat { get; }

    }
}
