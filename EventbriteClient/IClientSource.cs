﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventbriteClient
{
    public interface IClientSource
    {
        // requests data from a server
        // - requestUrl specifies the endpoint for the request
        // and the data to read
        // - returnes the string of data in the form of json objects
        string Read(Uri requestUrl);
       Task<String> ReadAsync(Uri requestUrl);
        
        // if we want to avoid creating
        // additional state machine and 
        // don't need continuation;
        // caller is responsible for awaiting the task.
        Task<String> ReadTask(Uri requestUrl);

        void SetKey(string key); 

        string Key { get; }
    }
}
