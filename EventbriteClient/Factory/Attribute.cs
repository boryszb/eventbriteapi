﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventbriteClient.Factory
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public class CreateWithNameAttribute : Attribute
    {
        public CreateWithNameAttribute(string name) => Name = name;
        public string Name { get; private set; }
    }
}
