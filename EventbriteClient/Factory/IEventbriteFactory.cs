﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventbriteClient.Factory
{
    public interface IEventbriteFactory<T> where T : class
    {
        T GetInstance(string key, params object[] vals);
        T GetInstance(params object[] vals);
    }
}
