﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventbriteClient
{
    // contract for implementing functionality of counstructing a url string send to a evenbrite server
    public interface IUrlBuilder : IParametersAdapter
    {
        IUrlBuilder AddQueryString(string query);
        IUrlBuilder AddQueryString(Action<IParametersAdapter> parameters);
        IUrlBuilder AddRequestMethod(string request);

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1054:Uri parameters should not be strings", Justification = "<Pending>")]
        IUrlBuilder AddRequestBaseUrl(string baseUrl = "");
        IUrlBuilder Expand(bool withTicket = true);
        Uri Build();
    }

    public interface IParametersAdapter
    {
        string this[string key] { get; set; }
        void ClearParameters();
    }
}
