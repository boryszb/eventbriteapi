﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventbriteClient.Adapter
{
    public interface IDataAdapter
    {
        IEventBriteData Root { get; }
        bool IsErrorMessage();
    }
}
