﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventbriteClient.Adapter
{
    public interface IEventBriteData
    {
        IEnumerable<IEventBriteData> Elements();
        string Name { get; set; }
        string Value { get; set; }
    }
}
