﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Text;
using EventbriteClient.Factory;
using EventbriteClient.Util;

namespace EventbriteClient.Adapter
{
    [CreateWithName(MessageFormat.XML)]
    public class XmlDocumentAdapter : IDataAdapter
    {
        readonly private IEventBriteData _root;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "<Pending>")]
        public XmlDocumentAdapter(string doc)
        {
            if (String.IsNullOrEmpty(doc)) 
                throw new ArgumentNullException(nameof(doc));

            XDocument document;
            try
            {
                document = XDocument.Parse(doc);
            }
            catch (XmlException)
            {
                // remove illegal characters
                document = XDocument.Parse(CleanXml(doc));
   
            }

            _root = new Item(document.Root);
        }

        private string CleanXml(string doc)
        {
            StringBuilder buffer = new StringBuilder(doc.Length);
            foreach (char c in doc)
            {
                if (IsLegal(c))
                {
                    buffer.Append(c);
                }
            }
            return buffer.ToString();
        }

       static private bool IsLegal(int c)
            => (c == 0x9 || c == 0xA || c == 0xD ||
               (c >= 0x20 && c <= 0xD7FF) ||
               (c >= 0xE000 && c <= 0xFFFD) ||
               (c >= 0x10000 && c <= 0x10FFFF)
                );
        #region IDataAdapter Members

        public IEventBriteData Root { get => _root; }
       
        #endregion
        public bool IsErrorMessage() => _root.Name == "error";
      
        private class Item : IEventBriteData
        {

            readonly XElement _element;
            readonly IEventBriteData[] _members;

            public Item(XElement element)
            {
                _element = element;
                _members = new IEventBriteData[_element.Elements().Count()];
                int counter = 0;
                foreach (var ele in _element.Elements())
                {
                    _members[counter++] = new Item(ele);
                }

            }

            #region IEventBriteData Members

            public IEnumerable<IEventBriteData> Elements() => _members;
            
            public string Name
            {
                get => _element.Name.LocalName;
                set => throw new NotImplementedException();
            }

            public string Value
            {
                get => _element.Value;
                set => _element.Value = value;
            }

            #endregion
        }
    }
}
