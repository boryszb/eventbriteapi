﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using EventbriteClient.Util;
using EventbriteClient.Factory;

namespace EventbriteClient.Adapter
{
    [CreateWithName(MessageFormat.JSON)]
    public class JsonAdapter : IDataAdapter
    {

        readonly private IEventBriteData _root;
        public JsonAdapter(string doc)
        {
            var _document = new JsonReader(doc).Parse() as Dictionary<string, object>;
            _root = _document == null ? null : new Item(_document);
        }
       
        public IEventBriteData Root {get => _root; }

        public bool IsErrorMessage() => _root.Name == "error" || _root.Elements().FirstOrDefault(e => e.Name == "error") != null;
        
        private class Item : IEventBriteData
        {
            private const string LIST_MEMBER = "list_member";
            private const string NESTED_ARRAY_ERROR = "Nested arrays are not supported";

            readonly List<IEventBriteData> _members;
            string _name;
            string _value;

            Item(string value)
            {
                _name = LIST_MEMBER;
                _value = value;
            }
            public Item(string name, string value)
            {
                _name = name;
                _value = value;
            }

            public Item(string name, ArrayList list)
            {
                _members = new List<IEventBriteData>();
                _name = name;
                foreach (var item in list)
                {
                    switch (item)
                    {
                        case Dictionary<string, object> dic:
                            LoadList(dic);
                            break;
                        case ArrayList arry:
                            throw new System.ArgumentException(NESTED_ARRAY_ERROR, nameof(list));
                        case string o:
                            _members.Add(new Item(o));
                            break;
                        case ValueType v:
                            _members.Add(new Item(v.ToString()));
                            break;
                        default: break;
                    }
                 }
            }

            public Item(string name, Dictionary<string, object> elements)
            {
                _members = new List<IEventBriteData>();
                _name = name;
                LoadList(elements);
            }

            public Item(Dictionary<string, object> elements)
            {
                _members = new List<IEventBriteData>();
                foreach (var item in elements)
                {
                    _name = item.Key;
                    var val = item.Value;

                    if (val is ArrayList)
                    {
                        var list = val as ArrayList;
                        for (int i = 0; i < list.Count; i++)
                        {
                            LoadList(list[i] as Dictionary<string, object>);
                        }
                    }
                    else 
                    {
                       LoadList(val as Dictionary<string, object>);
                    }
                }
            }

            private void LoadList(Dictionary<string, object> item)
            {
                if (item == null)
                    return;

                foreach (var ele in item)
                {
                    if (ele.Value is Dictionary<string, object>)
                        _members.Add(new Item(ele.Key, ele.Value as Dictionary<string, object>));
                    else if (ele.Value is ArrayList)
                        _members.Add(new Item(ele.Key, ele.Value as ArrayList));
                    else
                        _members.Add(new Item(ele.Key, ele.Value?.ToString()));
                }
            }

            #region IEventBriteData Members

            public IEnumerable<IEventBriteData> Elements() => _members;
            
            public string Name
            {
                get => _name;
                set => _name = value;
            }

            public string Value
            {
                get => _value;
                set => _value = value;
            }

            #endregion
        }
    }
}
