﻿using System;
using System.Net;
using System.IO;
using System.Threading.Tasks;

namespace EventbriteClient
{
    public class WebSource : IClientSource
    {
        string _key;
        public WebSource()
        {
        }
        public WebSource(string key)
        {
            SetKey(key);
        }
        #region IClientSource Members
        public string Key => _key;
        public void SetKey(string value)
        {
            _key = $"Bearer {value}"; 
        }
        public string Read(Uri requestUrl)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            using (WebClient client = new WebClient())
            {
                client.Headers.Add("Accept", "application/json");
                client.Headers.Add("Authorization", _key);
                
                using (StreamReader reader = new StreamReader(client.OpenRead(requestUrl.AbsoluteUri)))
                {
                    var response = reader.ReadToEnd();
                    return response;
                }
            }
        }

        public async Task<String> ReadAsync(Uri requestUrl)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            using (WebClient client = new WebClient())
            {
                client.Headers.Add("Accept", "application/json");
                client.Headers.Add("Authorization", _key);

                using (StreamReader reader = new StreamReader(await client.OpenReadTaskAsync(requestUrl.AbsoluteUri).ConfigureAwait(false)))
                {
                    return await reader.ReadToEndAsync().ConfigureAwait(false);
                }
            }
            
        }

        public Task<String> ReadTask(Uri requestUrl)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            using (WebClient client = new WebClient())
            {
                client.Headers.Add("Accept", "application/json");
                client.Headers.Add("Authorization", _key);

                return client.DownloadStringTaskAsync(requestUrl);
            }
        }

        #endregion
    }
}
