﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventbriteClient.Model
{
    public class Location
    {
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public string Region { get; set; }
        public string CountryName { get; set; }
        public string Country { get; set; }
        public string Longitude { get; set; }
        public string Latitude { get; set; }
        // public string Phone { get; set; }

        public void Deconstruct(out string address1, out string city, out string countryName)
            => (address1, city, countryName) = (Address1, City, CountryName);

        public void Deconstruct(out string longitude, out string latitude)
            => (longitude, latitude) = (Longitude, Latitude);
    }
}
