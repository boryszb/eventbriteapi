﻿using System;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventbriteClient.Model
{
    [Serializable]
    public sealed class EventbriteException : Exception, ISerializable
    {
        readonly private string _type;
        readonly private string _message;

        public string ErrorType { get { return _type; } }
        public string ErrorMessage { get { return _message; } }

        public override string Message
        {
            get
            {
               return FormattableString.Invariant($"{ErrorType}: {ErrorMessage}");
            }
        }

        public EventbriteException() { }
        public EventbriteException(String message) : base(message) { }
        public EventbriteException(String message, Exception innerException) : base(message, innerException) { }

        public EventbriteException(String eventbriteError, String errorType)
        {
            _type = errorType;
            _message = eventbriteError;
        }

        private EventbriteException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
            _type = info.GetString("ErrorType");
            _message = info.GetString("ErrorMessage");
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValue("ErrorType", _type);
            info.AddValue("ErrorMessage", _message);
        }
    }
}
