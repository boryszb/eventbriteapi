﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventbriteClient.Model
{
    public class User
    {
        public User()
        {
            Emails = new List<EmailInfo>();
        }

        public long? Id { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Name { get; set; }
        public List<EmailInfo> Emails { get; private set; }

        public void Deconstruct(out long? id, out string email, out string fname, out string lname)
            => (id, email, fname, lname) = (Id, Email, FirstName, LastName);
    }

    public class EmailInfo
    {
        public string Email { get; set; }
        public bool Verified { get; set; }
       
    }
}
