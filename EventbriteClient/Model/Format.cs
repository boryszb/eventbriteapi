﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventbriteClient.Model
{
    public class Format
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }

        public void Deconstruct(out int? id, out string name) => (id, name) = (Id, Name);
    }
}
