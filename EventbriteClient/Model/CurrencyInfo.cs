﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventbriteClient.Model
{
    public class CurrencyInfo
    {
        public string Currency { get; set; }
        public int? Value { get; set; }
        public string Display { get; set; }

        public void Deconstruct( out string currency, out int? val, out string display)
            => (currency, val, display) = (Currency, Value, Display);

       
    }
}
