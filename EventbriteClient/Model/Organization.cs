﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventbriteClient.Model
{
    public class Organization
    {
        public long ID { get; set; }
        public string Name { get; set; }
        public long? ImageId { get; set; }
        public string Vertical { get; set; }

        public Summary Pagination { get; set; }
    }
}
