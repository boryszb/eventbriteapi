﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventbriteClient.Model
{
    public class Order
    {

        public Order()
        {
            Attendees = new List<Attendee>();
            Costs = new OrderCosts();
        }

        public List<Attendee> Attendees { get; private set; }
        public DateTime? Created { get; set; }
        public string Email { get; set; }
        public Event Event { get; set; }
        public string Name { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? Changed { get; set; }
        public OrderCosts Costs { get; private set; }

        public void Deconstruct(out DateTime? created, out string name, out DateTime? changed)
            => (created, name, changed) = (Created, Name, Changed);
        
       
    }
    public class OrderCosts
    {
        public CurrencyInfo Gross { get; set; }
        public CurrencyInfo EventBriteFee { get; set; }
        public CurrencyInfo PaymentFee { get; set; }
        public CurrencyInfo Tax { get; set; }
    }
}
