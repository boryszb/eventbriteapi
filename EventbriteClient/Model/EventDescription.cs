﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventbriteClient.Model
{
    public class EventDescription
    {
        private string _html;
        public string Description { get => _html; set => _html = value; }
    }
}
