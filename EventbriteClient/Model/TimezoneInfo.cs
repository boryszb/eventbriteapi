﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventbriteClient.Model
{
    public class TimezoneInfo
    {
        private string _timezone;
        private string _label;
        public string Timezone 
        { 
            get => _timezone; 
            set => _timezone = value; 
        }
        public string Label 
        { 
            get => _label;
            set => _label = value;
        }
    }
}
