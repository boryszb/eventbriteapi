﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventbriteClient.Model
{
    public class Organizer
    {
        public Organizer() { Description = new MultipartText(); }
        public long? Id { get; set; }
        public string Name { get; set; }
        public MultipartText Description { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1056:Uri properties should not be strings", Justification = "<Pending>")]
        public string Url { get; set; }
        public int? NumPastEvents { get; set; }
        public int? NumFutureEvents { get; set; }

        public long? OrganizationId { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1054:Uri parameters should not be strings", Justification = "<Pending>")]
        public void Deconstruct(out long? id, out string name, out string url)
            => (id, name, url) = (Id, Name, Url);
    }
}
