﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventbriteClient.Model
{
    public class DateTimeTZ
    {
        public string TimeZone { get; set; }
        public string Utc { get; set; }
        public string Local { get; set; }

        public string DisplayLocal()
        {
            return Local.Replace('T', ' ');
        }

        public void Deconstruct(out string tz, out string utc, out string local)
            => (tz, utc, local) = (TimeZone, Utc, Local.Replace('T', ' '));
    }
}
