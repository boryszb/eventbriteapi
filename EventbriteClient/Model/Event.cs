﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventbriteClient.Model
{
    public enum Privacy
    {
        Private,
        Public
    }

    public enum Status
    {
        Draft,
        Live,
        Started,
        Ended,
        Canceled,
        Completed,
        Unsaved,
        Deleted
    }

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1716:Identifiers should not match keywords", Justification = "<Pending>")]
    public class Event
    {
        public Event()
        {
            this.Venue = new Venue();
            this.Organizer = new Organizer();
            this.Category = new Category();
            this.SubCategory = new SubCategory();
            this.Format = new Format();
            this.Name = new MultipartText();
            this.Description = new MultipartText();
            this.Start = new DateTimeTZ();
            this.End = new DateTimeTZ();
            this.Privacy = new Privacy();
            this.Logo = new Logo();
            this.TicketClasses = new List<Ticket>();
        }
        public long? Id { get; set; }
        public MultipartText Name { get; set; }
        public MultipartText Description { get; set; }
        public DateTimeTZ Start { get; set; }
        public DateTimeTZ End { get; set; }
        public string Currency { get; set; }
        public DateTime? Created { get; set; }
        public DateTime? Changed { get; set; }
        public Privacy Privacy { get; set; }
        public int? Capacity { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1056:Uri properties should not be strings", Justification = "<Pending>")]
        public string Url { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1056:Uri properties should not be strings", Justification = "<Pending>")]
        public string LogoUrl { get; set; }
        public string LogoId { get; set; }
        public long? OrganizerId { get; set; }
        public long? VenueId { get; set; }
        public int? CategoryId { get; set; }
        public int? SubCategoryId { get; set; }
        public int? FormatId { get; set; }

        public bool? OnlineEvent { get; set; }

        public string Status { get; set; }
        public Venue Venue { get; set; }
        public Category Category { get; private set; }
        public SubCategory SubCategory { get; private set; }
        public Format Format { get; private set; }
        public Organizer Organizer { get; set; }
        public Logo Logo { get; private set; }
        public List<Ticket> TicketClasses { get; private set; }
        public Summary Pagination { get; set; }

        public EventSearchRequest Request { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1054:Uri parameters should not be strings", Justification = "<Pending>")]
        public void Deconstruct(out long? id, out DateTime? created, out DateTime? changed, out string url)
            => (id, created, changed, url) = (Id, Created, Changed, Url);

        public void Deconstruct(out long? orgid, out long? venueid, out int? categoryid)
            => (orgid, venueid, categoryid) = (OrganizerId, VenueId, CategoryId);
    }

    public class Summary
    {
        public long? ObjectCount { get; set; }
        public long? PageNumber { get; set; }
        public int? PageSize { get; set; }
        public int? PageCount { get; set; }

    }

    public class EventSearchRequest
    {
        public Categories Categories { get; set; }
        public Formats Formats { get; set; }
        // public string Address { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        // public string PostalCode  {get; set;}
        public string Country { get; set; }
        public string Within { get; set; }

        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public DateKeywords DateKeyword { get; set; } // start date
        public DateKeywords DateModifiedKeyword { get; set; }
        public long Organizer { get; set; }
        public int Page { get; set; }
        public string Keywords { get; set; }
        // for sorting only
        public DateTime ByDate { get; }
       
        //  public long User { get; set; }
       
        // we don't support this fild anymore
      //  public DateTime? Date { get; set; } // start date
       
        
        // we support only date keywords
       // public DateTime? DateModified { get; set; }
       
        
       // public long SinceId { get; set; }
       

    }

    [Flags]
    public enum Formats
    {
        Conference = 1 << 1,
        Seminar = 1 << 2,
        Expo = 1 << 3,
        Convention = 1 << 4,
        Festival = 1 << 5,
        Performance = 1 << 6,
        Screening = 1 << 7,
        Gala = 1 << 8,
        Class = 1 << 9,
        Networking = 1 << 10,
        Party = 1 << 11,
        Rally = 1 << 12,
        Tournament = 1 << 13,
        Game = 1 << 14,
        Race = 1 << 15,
        Tour = 1 << 16,
        Attraction = 1 << 17,
        Retreat = 1 << 18,
        Appearance = 1 << 19,
        Other = 1 << 20
    }

    [Flags]
    public enum Categories
    {
        None = 0,
        Business = 101,
        ScienceTech,
        Music,
        FilmMedia,
        Arts,
        Fashion,
        Health,
        SportsFitness,
        TravelOutdoor,
        FoodDrink,
        CharityCauses,
        Government,
        Community,
        Spirituality,
        FamilyEducation,
        Holiday,
        HomeLifestyle,
        AutoBoatAir,
        Hobbies,
        SchoolActivities,
        Other = 199
    }

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1717:Only FlagsAttribute enums should have plural names", Justification = "<Pending>")]
    public enum DateKeywords
    {
        Today,
        Tomorrow,
        ThisWeek,
        ThisWeekend,
        NextWeek,
        ThisMonth,
        NextMonth
    }
}
