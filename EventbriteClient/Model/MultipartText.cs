﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventbriteClient.Model
{
    public class MultipartText
    {
        private string _text;
        private string _html;
        public string Text 
        { 
            get => _text;
            set => _text = value;
        }
        public string Html 
        { 
            get => _html; 
            set => _html = value;
        }
    }
}
