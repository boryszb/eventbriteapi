﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventbriteClient.Model
{
    public class Venue
    {
        public Venue()
        {
            Address = new Location();
        }
        public long? Id { get; set; }
        public string Name { get; set; }
        public Location Address { get; set; }
        public string Longitude { get; set; }
        public string Latitude { get; set; }

        public void Deconstruct(out long? id, out string name, out string longitude, out string latitude)
            => (id, name, longitude, latitude) = (Id, Name, Longitude, Latitude);
    }
}
