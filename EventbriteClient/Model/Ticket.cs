﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventbriteClient.Model
{
    public class Ticket
    {
        public long? Id { get; set; }
        public long? EventId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? MaximumQuantity { get; set; }
        public int? MinimumQuantity { get; set; }
        public CurrencyInfo Cost { get; set; }
        public CurrencyInfo Fee { get; set; }
        public CurrencyInfo Tax { get; set; }
        public bool? Donation { get; set; }
        public bool? Free { get; set; }
        public string OnSaleStatus { get; set; }

        public void Deconstruct(out long? id, out long? eventid, out string name, out string description)
            => (id, eventid, name, description) = (Id, EventId, Name, Description);
    }
}
