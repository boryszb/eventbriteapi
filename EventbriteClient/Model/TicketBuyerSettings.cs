﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventbriteClient.Model
{
    public class TicketBuyerSettings
    {
        public MultipartText ConfirmationMessage { get; set; }
        public MultipartText Instructions { get; set; }
        public long? EventId { get; set; }
        public bool? RefoundRequestEnabled { get; set; }
        public string RedirectUrl { get; set; }
    }
}
