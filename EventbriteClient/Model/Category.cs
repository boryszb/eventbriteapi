﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventbriteClient.Model
{
    public class Category
    {
        public int? Id { get; set; }
        public Category() { SubCategories = new List<SubCategory>(); }
        public string Name { get; set; }
        public string NameLocalized { get; set; }
        public string ShortName { get; set; }
        public List<SubCategory> SubCategories { get; private set; }

        public void Deconstruct(out int? id, out string name)
            => (id, name) = (Id, Name);
    }

    public class SubCategory
    {
        public int? Id { get; set; }
        public SubCategory() { }
        public string Name { get; set; }
        
        public void Deconstruct(out int? id, out string name)
            => (id, name) = (Id, Name);
    }
}
