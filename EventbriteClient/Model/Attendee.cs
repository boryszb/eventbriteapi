﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventbriteClient.Model
{
    //The Attendee object represents the details of Attendee (ticket holder to an Event).
    // Attendee objects are considered private
    public class Attendee
    {
        public Attendee()
        {
            Answers = new List<AttendeeAnswer>();
            Barcodes = new List<AttendeeBarcode>();
            Profile = new AttendeeProfile();
            Addresses = new AttendeeAddresses();
            Team = new AttendeeTeam();
        }

        public long Id { get; set; }
        public long? EventId { get; set; }
        public long? OrderId { get; set; }
        public long? TicketClassId { get; set; }
        public DateTime? Created { get; set; }
        public DateTime? Changed { get; set; }
        public AttendeeProfile Profile { get; private set; }
        public AttendeeAddresses Addresses { get; private set; }
        public List<AttendeeAnswer> Answers { get; private set; }
        public List<AttendeeBarcode> Barcodes { get; private set; }
        public AttendeeTeam Team { get; private set; }

        public void Deconstruct(out long id, out long? eventid, out long? orderid, out long? ticketid)
            => (id, eventid, orderid, ticketid) = (Id, EventId, OrderId, TicketClassId);

        public void Deconstruct(out long id, out DateTime? created, out DateTime? changed)
            => (id, created, changed) = (Id, Created, Changed);
        
    }
    public class AttendeeAddresses
    {
        public AttendeeAddresses()
        {
            Home = new Location();
            Ship = new Location();
            Work = new Location();
        }
        public Location Home { get; set; }
        public Location Ship { get; set; }
        public Location Work { get; set; }
    }
    public class AttendeeAnswer
    {
        public string Answer { get; set; }
        public string Question { get; set; }
        public int QuestionId { get; set; }
        public string Type { get; set; }

        public void Deconstruct(out string answer, out string question)
            => (answer, question) = (Answer, Question);
    }
    public class AttendeeBarcode
    {
        public string Id { get; set; }
        public string Status { get; set; }
        public string Barcode { get; set; }

        
    }
    public class AttendeeTeam
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public DateTime DateJoined { get; set; }
        public long? EventId { get; set; }

    }

    public class AttendeeProfile
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Prefix { get; set; }
        public string Suffix { get; set; }
        public int Age { get; set; }
        public string JobTitle { get; set; }
        public string Company { get; set; }
        public string Website { get; set; }
        public string Blog { get; set; }
        public string Gender { get; set; }
        public DateTime? BirthDate { get; set; }
        public string CellPhone { get; set; }
        // public string HomePhone { get; set; }
        // public string WorkPhone { get; set; }

        public void Deconstruct(out string fname, out string lname, out int age, out string gender)
           => (fname, lname, age, gender) = (FirstName, LastName, Age, Gender);
    }
}
