﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventbriteClient
{
    /// <summary>
    /// Based on Json parser in JavaScript found in JavaScript: The Good Parts.
    /// </summary>
    public sealed class JsonReader
    {
        int _pos;
        char _currentChar;
        readonly string _text;
        readonly Dictionary<char, char> escapee = new Dictionary<char, char>() {
                                                         { '"', '"' }, { '\\', '\\' },
                                                         { '/', '/' }, { 'n', '\n' },
                                                         { 'b', '\b' }, { 'f', '\f' },
                                                         { 'r', '\r' }, { 't', '\t' } };

        readonly Func<char, int, ArgumentException> _error = (t, p) => new ArgumentException(string.Format( CultureInfo.CurrentCulture,"Expected {0} in position: {1}", t, p));

        public JsonReader(string json)
        {
            _text = json;
            _pos = 0;
            _currentChar = Characters.EmptySpace;

        }

        public object Parse()
        {
            return Value();
        }

        private object ParseObject()
        {

            if (_currentChar == Characters.CurlyBracketOpen)
            {
                Next(Characters.CurlyBracketOpen);
                Skip();
                if (_currentChar == Characters.CurlyBracketClose)
                {
                    Next(Characters.CurlyBracketClose);
                    return null;
                }

                Dictionary<string, object> obj = new Dictionary<string, object>();
                while (_currentChar != Characters.EmptySpace)
                {
                    string key = ParseKeyString(); // it can be simplifyed
                    Skip();
                    Next(Characters.Colon);
                    obj[key] = Value();
                    Skip();

                    if (_currentChar == Characters.CurlyBracketClose)
                    {
                        Next(Characters.CurlyBracketClose);
                        return obj;
                    }
                    Next(Characters.Comma);
                    Skip();
                }

            }
            return null;
        }

        private object Value()
        {
            Skip();
            switch (_currentChar)
            {
                case Characters.CurlyBracketOpen:
                    var obj = new Dictionary<string, object>
                    {
                        { "jobject", ParseObject() }
                    };
                    return obj;
                case Characters.SquareBracketOpen:
                    return ParseArray();
                case Characters.Quotation:
                    return ParseString();
                case Characters.HyphenMinus:
                    return ParseNumber();
                default:
                    return Char.IsNumber(_currentChar) ? ParseNumber() : Word();
            }
        }

        private object ParseArray()
        {
            System.Collections.ArrayList al = new System.Collections.ArrayList();
            if (_currentChar == Characters.SquareBracketOpen)
            {
                Next(Characters.SquareBracketOpen);
                Skip();
                if (_currentChar == Characters.SquareBracketClose)
                {
                    Next(Characters.SquareBracketClose);
                    return al;
                }

                while (_currentChar != Characters.EmptySpace)
                {
                    al.Add(Value());
                    Skip();

                    if (_currentChar == Characters.SquareBracketClose)
                    {
                        Next(Characters.SquareBracketClose);
                        return al;
                    }
                    Next(Characters.Comma);
                    Skip();
                }

            }
            return al;

        }

        private object ParseNumber()
        {
            StringBuilder s = new StringBuilder();
            bool isInt = true;
           
            if (_currentChar == Characters.HyphenMinus)
            {
                // negative number
                s.Append(Characters.HyphenMinus);
                Next(Characters.HyphenMinus);
            }

            // build number
            while (Char.IsNumber(_currentChar))
            {
                s.Append(_currentChar);
                Next(null);
            }

            if (_currentChar == Characters.Dot)
            {
                // decimal point
                s.Append(_currentChar);

                // characters right of decimal point
                while (Next(null) != Char.MinValue && Char.IsNumber(_currentChar))
                {
                    s.Append(_currentChar);

                }
                isInt = false;
            }

            if (IsExponentialNotation())
            {
                s.Append(_currentChar);
                Next(null);
                if (IsPlusOrMinus())
                {
                    s.Append(_currentChar);
                    Next(null);
                }

                while (Char.IsNumber(_currentChar))
                {
                    s.Append(_currentChar);
                    Next(null);
                }
                
                return double.Parse(s.ToString(), NumberStyles.AllowExponent | NumberStyles.AllowDecimalPoint | NumberStyles.AllowLeadingSign,CultureInfo.InvariantCulture);
            }
            if (isInt)
                return long.Parse(s.ToString(),CultureInfo.InvariantCulture);

            return decimal.Parse(s.ToString(), NumberStyles.Any, CultureInfo.InvariantCulture);

        }

        // parsing key of event brite json document;
        // the key does not contain escapee and special characters
        private string ParseKeyString()
        {
            StringBuilder sb = new StringBuilder(55);
           // int start = _pos;
           
            if (_currentChar == Characters.Quotation)
            {
                while (Next(null) != char.MinValue)
                {
                    if (_currentChar == Characters.Quotation)
                    {
                        // end of string
                        Next(null);
                        return sb.ToString();
                    }
                    else
                    {
                        sb.Append(_currentChar);
                    }
                }
            }

            return sb.ToString();
        }
        private string ParseString()
        {
            StringBuilder s = new StringBuilder(255);

            if (_currentChar == Characters.Quotation)
            {
                while (Next(null) != char.MinValue)
                {
                    if (_currentChar == Characters.Quotation)
                    {
                        // end of string
                        Next(null);
                        return s.ToString();
                    }
                    else if (_currentChar == Characters.Backslash)
                    {
                        Next(null);
                        if (_currentChar == Characters.Unicode)
                        {
                            // build unicode value
                            int result = 0;
                            // int digit = 0;
                            for (int i = 0; i < 4; i++)
                            {
                                result = (result * 16) + GetDigit(Next(null));
                            }
                            s.Append(Convert.ToChar(result));
                        }
                        else if (escapee.ContainsKey(_currentChar))
                        {
                            // add special characters
                            s.Append(escapee[_currentChar]);
                        }
                        else
                        {
                            break;
                        }
                    }
                    else
                    {
                        // all cases covered, add current character
                        s.Append(_currentChar);
                    }
                }
            }
            return s.ToString();
        }

        private object Word()
        {
            switch (_currentChar)
            {
                case 't':
                    Next('t');
                    Next('r');
                    Next('u');
                    Next('e');
                    return true;
                case 'f':
                    Next('f');
                    Next('a');
                    Next('l');
                    Next('s');
                    Next('e');
                    return false;
                case 'n':
                    Next('n');
                    Next('u');
                    Next('l');
                    Next('l');
                    return null;
                default:
                    throw _error(_currentChar, _pos);
            }
        }

        #region Navigation members
        private void Skip()
        {
            while (_currentChar <= Characters.EmptySpace && _currentChar != char.MinValue)
            {
                Next(null);
            }
        }

        private char Next(char? c)
        {
            if (c.HasValue && c != _currentChar)
                throw _error(c.Value, _pos);

            if (_pos < _text.Length)
            {
                _currentChar = _text[_pos];
                _pos++;
            }
            else
            {
                _currentChar = char.MinValue;
            }
            return _currentChar;

        }
        #endregion

        #region Conditions
       
        private bool IsExponentialNotation()
        {
            return _currentChar == 'e' || _currentChar == 'E';
        }

        private bool IsPlusOrMinus()
        {
            return _currentChar == Characters.HyphenMinus || _currentChar == Characters.Plus;
        }

        #endregion

        private static class Characters
        {
            public const char Comma = ',';
            public const char Dot = '.';
            public const char SquareBracketClose = ']';
            public const char SquareBracketOpen = '[';
            public const char CurlyBracketOpen = '{';
            public const char CurlyBracketClose = '}';
            public const char EmptySpace = ' ';
            public const char Zero = '0';
            public const char Nine = '9';
            public const char HyphenMinus = '-';
            public const char Plus = '+';
            public const char Quotation = '"';
            public const char Backslash = '\\';
            public const char Unicode = 'u';
            public const char Colon = ':';
        }

        private static int GetDigit(char c)
        {
            switch (c)
            {
                case 'a': return 0x0A;
                case 'b': return 0x0B;
                case 'c': return 0x0C;
                case 'd': return 0x0D;
                case 'e': return 0x0E;
                case 'f': return 0x0F;
                default: return c - '0';
            }
        }
    }
}
