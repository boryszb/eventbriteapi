﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventbriteClient
{
    public interface IMapper<T>
    {
        T BuildInstance();
        IEnumerable<T> Build();
    }

}
