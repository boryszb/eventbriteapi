﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using EventbriteClient.Model;
using EventbriteClient.Util;
using EventbriteClient.Adapter;
using EventbriteClient.Factory;
using System.Threading.Tasks;

namespace EventbriteClient
{
    public class Client : IEventbriteClient, IEventBriteConfiguration
    {
        readonly IEventbriteFactory<IDataAdapter> _dataAdapterFactory;
        readonly IUrlBuilder _urlBuilder;
        readonly IClientSource _source;

        public Client()
        {
            _urlBuilder = new EventbriteFactory<IUrlBuilder>().GetInstance();
            _dataAdapterFactory = new EventbriteFactory<IDataAdapter>();
            _source = new EventbriteFactory<IClientSource>().GetInstance();
        }

        public string UserKey { get; set; }
        public string AppKey { get; set; }
        public TextWriter LogResult { get; set; }
        public bool AddTickets { get; set; }


        public string GetResponse(Uri requestUrl)
        {
            _source.SetKey(AppKey);
            string doc = _source.Read(requestUrl);
            if (LogResult != null)
                LogResult.Write(doc);
            return doc;
        }

        public async Task<String> GetResponseAsync(Uri requestUrl)
        {
            if (requestUrl == null) throw new ArgumentNullException(nameof(requestUrl));
            _source.SetKey(AppKey);
            bool writetoLog = LogResult != null;
            var doc = await _source.ReadAsync(requestUrl).ConfigureAwait(false);

            if (writetoLog) LogResult.Write(doc);

            return doc;

        }

        public Logo GetMediaObject(long id)
        {
            var requestString = CreateRequestForMethod(String.Format(CultureInfo.InvariantCulture, APIMethods.Media, id))
                                        .Build();
            return MapInstance<Logo>(GetResponse(requestString));
        }
        public IEnumerable<Format> GetFormats()
        {
            var requestString = CreateRequestForMethod(APIMethods.GetFormats)
                                        .Build();
            return Map<Format>(GetResponse(requestString));
        }

        public IEnumerable<Category> GetCategories()
        {
            var requestString = CreateRequestForMethod(APIMethods.GetCategories)
                                         .Build();
            return Map<Category>(GetResponse(requestString));
        }

        public Category GetCategory(int id)
        {
            var requestString = CreateRequestForMethod(String.Format(CultureInfo.InvariantCulture, APIMethods.GetCategory, id))
                                        .Build();
            return MapInstance<Category>(GetResponse(requestString));
        }

        public IEnumerable<Organization> GetUserOrganizations(long? id)
        {
            Uri requestString = CreateRequestForMethod(String.Format(CultureInfo.InvariantCulture, APIMethods.UserOrganizations, id == null ? "me" : id.Value.ToString(CultureInfo.InvariantCulture)))
                                        .Build();
            return Map<Organization>(GetResponse(requestString));
        }
        public IEnumerable<Order> GetUserOrders(long? id)
        {
            Uri requestString = CreateRequestForMethod(String.Format(CultureInfo.InvariantCulture, APIMethods.GetUserOrder, id == null ? "me" : id.Value.ToString(CultureInfo.InvariantCulture)))
                                         .Build();
            return Map<Order>(GetResponse(requestString));
        }

        public IEnumerable<Order> GetEventOrders(long eventId)
        {
            var requestString = CreateRequestForMethod(String.Format(CultureInfo.InvariantCulture, APIMethods.EventOrders, eventId))
                                           .Build();
            return Map<Order>(GetResponse(requestString));
        }

        public Order GetOrder(long id)
        {
            var requestString = CreateRequestForMethod(String.Format(CultureInfo.InvariantCulture, APIMethods.GetOrder, id))
                                         .Build();

            return MapInstance<Order>(GetResponse(requestString));
        }


        public IEnumerable<Attendee> EventAttendees(long eventId)
        {

            var requestString = CreateRequestForMethod(String.Format(CultureInfo.InvariantCulture, APIMethods.EventListAttendees, eventId))
                                          .Build();

            return Map<Attendee>(GetResponse(requestString));
        }

        public Event GetEvent(long eventId)
        {

            var requestString = CreateRequestForMethod(String.Format(CultureInfo.InvariantCulture, APIMethods.EventGet, eventId)).Expand().Build();

            return MapInstance<Event>(GetResponse(requestString));
        }

        public Venue GetVenue(int venueId)
        {
            var requestString = CreateRequestForMethod(String.Format(CultureInfo.InvariantCulture, APIMethods.VenueGet, venueId))
                                         .Build();

            return MapInstance<Venue>(GetResponse(requestString));
        }

        public IEnumerable<Event> GetEventsByOrganization(long id)
        {
            var requestString = CreateRequestForMethod(string.Format(CultureInfo.InvariantCulture, APIMethods.GetEventsByOrganization, id))
                                         .Build();

            return Map<Event>(GetResponse(requestString));
        }
        public IEnumerable<Event> GetUserEvents()
        {
            var requestString = CreateRequestForMethod(String.Format(CultureInfo.InvariantCulture, APIMethods.UserListEvents, "me"))
                                          .Build();

            return Map<Event>(GetResponse(requestString));
        }
        public User GetUser()
        {
            var requestString = CreateRequestForMethod(String.Format(CultureInfo.InvariantCulture, APIMethods.UserGet, "me")).Build();

            return MapInstance<User>(GetResponse(requestString));
        }
        public IEnumerable<Venue> GetVenuesByOrganization(long orgId)
        {
            var requestString = CreateRequestForMethod(String.Format(CultureInfo.InvariantCulture, APIMethods.VenuesByOrganization, orgId)).Build();

            return Map<Venue>(GetResponse(requestString));
        }
        public IEnumerable<Event> GetEventsByVenue(long id)
        {
            var requestString = CreateRequestForMethod(String.Format(CultureInfo.InvariantCulture, APIMethods.GetEventsByVenue, id)).Build();

            return Map<Event>(GetResponse(requestString));
        }

        public IEnumerable<Attendee> GetAttendeesByOrganization(long id)
        {
            var requestString = CreateRequestForMethod(String.Format(CultureInfo.InvariantCulture, APIMethods.GetAttendeesByOrganization, id)).Build();

            return Map<Attendee>(GetResponse(requestString));
        }

        public IEnumerable<Event> GetEventsBySeries(long id)
        {
            var requestString = CreateRequestForMethod(String.Format(CultureInfo.InvariantCulture, APIMethods.GetEventsBySeries, id)).Build();

            return Map<Event>(GetResponse(requestString));
        }
        public EventDescription GetEventDescription(long id)
        {
            var requestString = CreateRequestForMethod(String.Format(CultureInfo.InvariantCulture, APIMethods.EventDescription, id)).Build();

            return MapInstance<EventDescription>(GetResponse(requestString));
        }

        public TicketBuyerSettings GetTicketBuyerSettings(long id)
        {
            var requestString = CreateRequestForMethod(String.Format(CultureInfo.InvariantCulture, APIMethods.TicketBuyerSettings, id)).Build();

            return MapInstance<TicketBuyerSettings>(GetResponse(requestString));
        }

        public IEnumerable<Ticket> GetTicketsByEvent(long eventId)
        {
            var requestString = CreateRequestForMethod(String.Format(CultureInfo.InvariantCulture, APIMethods.TicketClassesByEvent, eventId)).Build();
            return Map<Ticket>(GetResponse(requestString));

        }
        public Ticket GetTicketBytTicketId(long eventId, long ticketId)
        {
            var requestString = CreateRequestForMethod(String.Format(CultureInfo.InvariantCulture, APIMethods.TicketClass, eventId, ticketId)).Build();

            return MapInstance<Ticket>(GetResponse(requestString));
        }

        public IUrlBuilder CreateRequestForMethod(string command)
        {
            return _urlBuilder.AddRequestBaseUrl()
                               .AddRequestMethod(command);
        }

        public IDataAdapter CreateAdapter(string doc)
        {
            var data = _dataAdapterFactory.GetInstance(MessageFormat.JSON, doc);

            if (data.IsErrorMessage())
            {
                var errorMessage = data.Root.Elements().First(e => e.Name == "error_message").Value;
                var errorType = data.Root.Elements().First(e => e.Name == "error_type").Value;
                throw new EventbriteException(errorMessage, errorType);
            }

            return data;
        }

        private IEnumerable<T> Map<T>(string doc)
        {
            IDataAdapter adapter = CreateAdapter(doc);
            Mapper<T> mapper = new Mapper<T>(adapter);
            return mapper.Build();
        }

        private T MapInstance<T>(string doc)
        {
            IDataAdapter adapter = CreateAdapter(doc);
            Mapper<T> mapper = new Mapper<T>(adapter);
            return mapper.BuildInstance();
        }
    }
}
