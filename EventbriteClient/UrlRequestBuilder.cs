﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using EventbriteClient.Util;

namespace EventbriteClient
{
    public class UrlRequestBuilder: IUrlBuilder
    {
        const string DefaultBaseUrl = "https://www.eventbriteapi.com/";

        readonly private IDictionary<string, string> _parameters;
        // info requiered to connect to eventbrite server
       // private IEventBriteConfiguration _configuration;
        private string _queryString;
        private string _requestMethod;
        private string _requestBase;
        private bool _expand = false;
        private bool _expandWithoutTicket = false;

        public UrlRequestBuilder(IDictionary<string, string> parameters)
        {
            _parameters = parameters;
           
        }

        public UrlRequestBuilder() : this(new Dictionary<string, string>()) { }

        public IUrlBuilder AddQueryString(string query)
        {
            if (String.IsNullOrEmpty(query))
                query = BuildQueryString();
            _queryString = query;

            return this;

        }

        public IUrlBuilder AddRequestMethod(string request)
        {
            _requestMethod = request;

            return this;
        }

        public IUrlBuilder AddRequestBaseUrl(string baseUrl = "")
        {
            if (String.IsNullOrEmpty(baseUrl))
                baseUrl = DefaultBaseUrl;

            _requestBase = baseUrl;

            return this;
        }

        //public IUrlBuilder AddConfiguration(IEventBriteConfiguration config)
        //{
        //    _configuration = config;

        //    return this;
        //}

        public IUrlBuilder AddQueryString(Action<IParametersAdapter> parameters)
        {
            parameters = parameters ?? throw new ArgumentNullException(nameof(parameters));
            parameters.Invoke(this);

            return this;
        }

        public string this[string key]
        {
            get
            {
                if (_parameters.ContainsKey(key))
                    return _parameters[key];
                return string.Empty;
            }
            set
            {
                if (_parameters.ContainsKey(key))
                {
                    _parameters[key] = value;
                }
                else
                {
                    _parameters.Add(key, value);
                }
            }
        }

        public void ClearParameters()
        {
            _parameters.Clear();
        }

        public Uri Build()
        {
            if (String.IsNullOrEmpty(_queryString))
            {
                _queryString = BuildQueryString();
            }

            return DoBuild();
        }

        private Uri DoBuild()
        {
            UriBuilder uri = new UriBuilder(_requestBase)
            {
                Path = string.Format(CultureInfo.CurrentCulture, "/v3/{0}/", _requestMethod),
                Port = -1
            };

            if (!String.IsNullOrEmpty(_queryString))
                uri.Query = string.Format(CultureInfo.CurrentCulture,"{0}{1}", uri.Query.TrimStart('?'), _queryString);

            if (_expand)
                uri.Query = string.Format(CultureInfo.CurrentCulture,"{0}&expand=organizer,venue,category,ticket_classes", uri.Query.TrimStart('?'));
            else if (_expandWithoutTicket)
                uri.Query = string.Format(CultureInfo.CurrentCulture, "{0}&expand=organizer,venue,category", uri.Query.TrimStart('?'));
            else
                uri.Query = string.Format(CultureInfo.CurrentCulture, "{0}", uri.Query.TrimStart('?'));

            // _configuration.RequestURL = uri.Uri.ToString();
            return uri.Uri;
        }

        private string BuildQueryString()
        {
            NameValueCollection queryString = HttpUtility.ParseQueryString("");

            foreach (var item in _parameters)
            {
                if (!String.IsNullOrEmpty(item.Value))
                {
                    queryString.Add(item.Key, item.Value);
                }
            }
            return queryString.ToString();
        }

        public IUrlBuilder Expand(bool withTicket)
        {
            if (withTicket)
                _expand = true;
            else
                _expandWithoutTicket = true;

            return this;
        }
    }
}
