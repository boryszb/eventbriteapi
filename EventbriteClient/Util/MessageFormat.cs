﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventbriteClient.Util
{
    public static class MessageFormat
    {
        public const string XML = "xml";
        public const string JSON = "json";
    }
}
