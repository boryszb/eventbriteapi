﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventbriteClient.Util
{
    public static class APIMethods
    {
        public const string EventSearch = "events/search";
        public const string EventListAttendees = "events/{0}/attendees";
        public const string UserListEvents = "users/{0}/owned_events";
        public const string UserListVenues = "users/{0}/venues";
        public const string UserListOrganizers = "users/{0}/organizers";
        public const string EventGet = "events/{0}";
        public const string VenueGet = "venues/{0}";
        public const string OrganizerGet = "organizers/{0}";
        public const string UserGet = "users/{0}";
        public const string EventQuestions = "events/{0}/questions";
        public const string EventOrders = "events/{0}/orders";
        public const string GetOrder = "orders/{0}";
        public const string GetUserOrder = "users/{0}/orders";
        public const string GetCategories = "categories";
        public const string GetCategory = "categories/{0}";
        public const string GetFormats = "formats";

        public const string GetEventsByVenue = "venues/{0}/events";
        public const string GetEventsByOrganization = "organizations/{0}/events";
        public const string GetAttendeesByOrganization = "organizations/{0}/attendees";
        public const string EventDescription = "events/{0}/description";
        public const string GetEventsBySeries = "series/{0}/events";
        public const string Media = "media/{0}";
        public const string UserOrganizations = "users/{0}/organizations";
        public const string VenuesByOrganization = "organizations/{0}/venues";
        public const string TicketBuyerSettings = "events/{0}/ticket_buyer_settings";
        public const string TicketClass = "events/{0}/ticket_classes/{1}";
        public const string TicketClassesByEvent = "events/{0}/ticket_classes";


    }
}
