﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Net;
using EventbriteClient;
using EventbriteClient.Model;
using EventbriteClient.Adapter;
using EventbriteClient.Util;

namespace ClientHarness
{
    class Program
    {
        static void Main(string[] args)
        {
            ParseAttendees();
            ParseCategories();
            ParseCategory();
            ParseCategoryExpended();
            ParseFormat();
            ParseFormatsList();
            ParseDescription();
            ParseEvent();
            ParseEventsList();
            ParseNoExtendedEventsList();
            ParseLogo();
            ParseOrganizations();
            ParseOrder();
            ParseTicketBuyerSettings();
            ParseTicketClass();
            ParseTicketsList();
            ParseVenue();

            Console.Read();
        }

        static void ParseAttendees()
        {

            string test = null;
            using (var reader = new StreamReader("../../tests/Attendees.txt"))
            {
               test = reader.ReadToEnd();
               
            } 
            IDataAdapter adapter = new JsonAdapter(test);
            var attendees = new Mapper<Attendee>(adapter).Build();
            foreach(var item in attendees)
            {
                Console.WriteLine($"{item.Id} {item.Profile.Name}");
            }
        }

        static void ParseCategories()
        {
            string test = null;
            using (var reader = new StreamReader("../../tests/Categories.txt"))
            {
                test = reader.ReadToEnd();
            }
               
            IDataAdapter adapter = new JsonAdapter(test);
            var categories = new Mapper<Category>(adapter).Build();

            foreach (var c in categories)
            {
                Console.WriteLine($"{c.Id} = {c.Name}");
            }
        }

        static void ParseCategory()
        {
            string test = null;
            using (var reader = new StreamReader("../../tests/Category.txt"))
            {
                test = reader.ReadToEnd();
            }
            IDataAdapter adapter = new JsonAdapter(test);

            Mapper<Category> mapper = new Mapper<Category>(adapter);
            var result = mapper.BuildInstance();
            (int? id, string name) = result;

            Console.WriteLine($"{id ?? 0}, {name}");
        }

        static void ParseCategoryExpended()
        {
            string test = null;
            using (var reader = new StreamReader("../../tests/CategoryExpended.txt"))
            {
                test = reader.ReadToEnd();
            }
               
            IDataAdapter adapter = new JsonAdapter(test);
            var category = new Mapper<Category>(adapter).BuildInstance();

            Console.WriteLine($"{category.Id} {category.Name}");
            foreach (var subcategory in category.SubCategories)
            {
                Console.WriteLine($"{subcategory.Id} {subcategory.Name}");
            }

        }
        static void ParseFormat()
        {
            string test = null;
            using (var reader = new StreamReader("../../tests/Format.txt"))
            {
                test = reader.ReadToEnd();
            }
                
            IDataAdapter adapter = new JsonAdapter(test);

            Mapper<Format> mapper = new Mapper<Format>(adapter);
            var result = mapper.BuildInstance();
            (int? id, string name) = result;

            Console.WriteLine($"{id ?? 0}, {name}");
        }

        static void ParseFormatsList()
        {
            string test = null;
            using (var reader = new StreamReader("../../tests/Formats.txt"))
            {
                test = reader.ReadToEnd();
            }
          
            IDataAdapter adapter = new JsonAdapter(test);

            Mapper<Format> mapper = new Mapper<Format>(adapter);
            var result = mapper.Build();
            foreach (var item in result)
            {
                Console.WriteLine($"{item.Id} {item.Name}");
            }
        }
        static void ParseEvent()
        {
            string test = null;
            using (var reader = new StreamReader("../../tests/Event.txt"))
            {
               test = reader.ReadToEnd();
            }
            IDataAdapter adapter = new JsonAdapter(test);

            Mapper<Event> mapper = new Mapper<Event>(adapter);
            var result = mapper.BuildInstance();

            Console.WriteLine($"{result.Id}, {result.Name}");
        }

        static void ParseEventsList()
        {
            string test = null;
            using (var reader = new StreamReader("../../tests/Events.txt"))
            {
                test = reader.ReadToEnd();
            }

            IDataAdapter adapter = new JsonAdapter(test);

            Mapper<Event> mapper = new Mapper<Event>(adapter);
            var list = mapper.Build();

            foreach (var result in list)
            {
                Console.WriteLine($"{result.Id}, {result.Name.Text}");
                Console.WriteLine($"organizer = {result.Organizer.Name}");
                Console.WriteLine($"venue = {result.Venue.Name}");
            }
        }

        static void ParseNoExtendedEventsList()
        {
            string test = null;
            using (var reader = new StreamReader("../../tests/EventsNoExtended.txt"))
            {
                test = reader.ReadToEnd();
            }

            IDataAdapter adapter = new JsonAdapter(test);

            Mapper<Event> mapper = new Mapper<Event>(adapter);
            var list = mapper.Build();

            foreach (var result in list)
            {
                Console.WriteLine($"{result.Id}, {result.Name.Text}");

            }
        }
        static void ParseDescription()
        {
            string test = null;
            using (var reader = new StreamReader("../../tests/description.txt"))
            {
                test = reader.ReadToEnd();
            }
            IDataAdapter adapter = new JsonAdapter(test);
            var description = new Mapper<EventDescription>(adapter).BuildInstance();
            Console.WriteLine(description.Description);
        }

        static void ParseLogo()
        {
            string test = null;
            using (var reader = new StreamReader("../../tests/Logo.txt"))
            {
                test = reader.ReadToEnd();
            }

            IDataAdapter adapter = new JsonAdapter(test);

            Mapper<Logo> mapper = new Mapper<Logo>(adapter);
            var result = mapper.BuildInstance();
            (long? id, string url) = result;

            Console.WriteLine($"{id ?? 0}, {url}");
        }

        static void ParseOrganizations()
        {
            string test = null;
            using (var reader = new StreamReader("../../tests/Organizations.txt"))
            {
                test = reader.ReadToEnd();
            }

            IDataAdapter adapter = new JsonAdapter(test);

            Mapper<Organization> mapper = new Mapper<Organization>(adapter);
            var result = mapper.Build();
            foreach (var item in result)
            {
                Console.WriteLine($"{item.ID} {item.Name}");
            }

        }
        static void ParseOrder()
        {
            string test = null;
            using (var reader = new StreamReader("../../tests/Order.txt"))
            {
                test = reader.ReadToEnd();
            }
            IDataAdapter adapter = new JsonAdapter(test);
            var order = new Mapper<Order>(adapter).BuildInstance();

            Console.WriteLine(order.FirstName + " " + order.Name + " " + order.Email);
        }

        static void ParseTicketsList()
        {
            string test = null;
            using (var reader = new StreamReader("../../tests/Tickets.txt"))
            {
                test = reader.ReadToEnd();
            }

            IDataAdapter adapter = new JsonAdapter(test);

            Mapper<Ticket> mapper = new Mapper<Ticket>(adapter);
            var result = mapper.Build().ToList();

            foreach (var item in result)
            {
                Console.WriteLine($"{item.Id} {item.Name}");
            }

        }

        static void ParseTicketClass()
        {
            string test = null;
            using (var reader = new StreamReader("../../tests/Ticket.txt"))
            {
                test = reader.ReadToEnd();
            }
            IDataAdapter adapter = new JsonAdapter(test);

            Mapper<Ticket> mapper = new Mapper<Ticket>(adapter);
            var result = mapper.BuildInstance();

            Console.WriteLine($"{result.Id} {result.Name}");

        }

        static void ParseTicketBuyerSettings()
        {
            string test = null;
            using (var reader = new StreamReader("../../tests/TicketBuyerSettings.txt"))
            {
                test = reader.ReadToEnd();
            }
            IDataAdapter adapter = new JsonAdapter(test);
            Mapper<TicketBuyerSettings> mapper = new Mapper<TicketBuyerSettings>(adapter);
            var result = mapper.BuildInstance();

            Console.WriteLine("{0} {1}", result.EventId, result.ConfirmationMessage.Text);
        }

        static void ParseVenue()
        {
            string test = null;
            using (var reader = new StreamReader("../../tests/Venue.txt"))
            {
                test = reader.ReadToEnd();
            }

            IDataAdapter adapter = new JsonAdapter(test);

            Mapper<Venue> mapper = new Mapper<Venue>(adapter);
            var result = mapper.BuildInstance();

            Console.WriteLine($"{result.Id}, {result.Name}");
        }

    }
}
