﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using EventbriteClient.Model;
using Eventbrite.LinqProvider;

namespace LinqProviderTests
{
    /// <summary>
    /// Summary description for ContextTests
    /// </summary>
    [TestClass]
    public class ContextTests
    {
        public ContextTests()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void CanQueryByCity()
        {
            EventbriteContext context = new EventbriteContext("xxxxxxx");

            var q = from e in context.Query
                    where e.Request.City == "qqqq"
                    select e;

            Assert.IsTrue(q.ToString() == "location.address=qqqq");
        }

        [TestMethod]
        public void CanQueryByCountry()
        {
            EventbriteContext context = new EventbriteContext("xxxxxxx");

            var q = from e in context.Query
                    where e.Request.Country == "US"
                    select e;

            Assert.IsTrue(q.ToString() == "location.address=US");
        }

        [TestMethod]
        public void CanQueryByRegion()
        {
            EventbriteContext context = new EventbriteContext("xxxxxxx");

            var q = from e in context.Query
                    where e.Request.Region == "IN"
                    select e;

            Assert.IsTrue(q.ToString() == "location.address=IN");
        }

        [TestMethod]
        public void CanQueryByAddress()
        {
            EventbriteContext context = new EventbriteContext("xxxxxxx");

            var q = from e in context.Query
                    where e.Request.Region == "IN" &
                    e.Request.Country == "US" &
                    e.Request.City == "Indianapolis"
                    select e;

            Assert.IsTrue(q.ToString() == "location.address=Indianapolis,IN,US");
        }

        [TestMethod]
        public void CanQueryByCategories()
        {
            EventbriteContext context = new EventbriteContext("xxxxxxx");

            var q = from e in context.Query
                    where e.Request.Region == "IN" &
                    e.Request.Country == "US" &
                    e.Request.City == "Indianapolis" &
                    e.Request.Categories == Categories.Business
                    select e;

            Assert.IsTrue(q.ToString() == "categories=101&location.address=Indianapolis,IN,US");
        }

        [TestMethod]
        public void CanQueryByDate()
        {
            EventbriteContext context = new EventbriteContext("xxxxxxx");

            var q = from e in context.Query
                    where e.Request.Region == "IN" &
                    e.Request.Country == "US" &
                    e.Request.City == "Indianapolis" &
                    e.Request.Categories == Categories.Business &
                    e.Request.DateKeyword == DateKeywords.NextMonth &
                    e.Request.DateModifiedKeyword == DateKeywords.Today
                    select e;

            Assert.IsTrue(q.ToString() == "categories=101&start_date.keyword=next_month&date_modified.keyword=today&location.address=Indianapolis,IN,US");
        }

        [TestMethod]
        public void CanQueryByOrganizer()
        {
            EventbriteContext context = new EventbriteContext("xxxxxxx");

            var q = from e in context.Query
                    where e.Request.Region == "IN" &
                    e.Request.Organizer == 1111 &
                    e.Request.Country == "US" &
                    e.Request.City == "Indianapolis" &
                    e.Request.Categories == Categories.Business &
                    e.Request.DateKeyword == DateKeywords.NextMonth &
                    e.Request.DateModifiedKeyword == DateKeywords.Today
                    select e;

            Assert.IsTrue(q.ToString() == "organizer.id=1111&categories=101&start_date.keyword=next_month&date_modified.keyword=today&location.address=Indianapolis,IN,US");
        }

        [TestMethod]
        public void CanQueryByLongitudeLatitude()
        {
            EventbriteContext context = new EventbriteContext("xxxxxxx");

            var q = from e in context.Query
                    where e.Request.Latitude == "111111" &
                    e.Request.Longitude == "222222222" &
                    e.Request.Within == "5km"
                    select e;

            Assert.IsTrue(q.ToString() == "location.latitude=111111&location.longitude=222222222&location.within=5km");
        }

        [TestMethod]
        public void CanQueryByPage()
        {
            EventbriteContext context = new EventbriteContext("xxxxxxx");

            var q = from e in context.Query
                    where e.Request.Latitude == "111111" &
                    e.Request.Longitude == "222222222" &
                    e.Request.Within == "5km" &
                    e.Request.Page == 2
                    select e;

            Assert.IsTrue(q.ToString() == "location.latitude=111111&location.longitude=222222222&location.within=5km&page=2");
        }

        [TestMethod]
        public void CanOrderQueryByDateAscending()
        {
            EventbriteContext context = new EventbriteContext("xxxxxxx");

            var q = from e in context.Query
                    where e.Request.Latitude == "111111" &
                    e.Request.Longitude == "222222222" &
                    e.Request.Within == "5km" &
                    e.Request.Page == 2
                    orderby e.Request.ByDate
                    select e;

            Assert.IsTrue(q.ToString() == "location.latitude=111111&location.longitude=222222222&location.within=5km&page=2&sort_by=ByDate");
        }

        [TestMethod]
        public void CanOrderQueryByDateDescending()
        {
            EventbriteContext context = new EventbriteContext("xxxxxxx");

            var q = from e in context.Query
                    where e.Request.Latitude == "111111" &
                    e.Request.Longitude == "222222222" &
                    e.Request.Within == "5km" &
                    e.Request.Page == 2
                    orderby e.Request.ByDate descending
                    select e;

            Assert.IsTrue(q.ToString() == "location.latitude=111111&location.longitude=222222222&location.within=5km&page=2&sort_by=-ByDate");
        }

        [TestMethod]
        public void CanExtendQueryWithWhere()
        {
            EventbriteContext context = new EventbriteContext("xxxxxxx");

            var q = from e in context.Query
                    where e.Request.Latitude == "111111" &
                    e.Request.Longitude == "222222222"
                    select e;

            q = q.Where(e => e.Request.Within == "10m");

            Assert.IsTrue(q.ToString() == "location.within=10m&location.latitude=111111&location.longitude=222222222");
        }

        [TestMethod]
        public void CanExtendQueryWithOrderBy()
        {
            EventbriteContext context = new EventbriteContext("xxxxxxx");

            var q = from e in context.Query
                    where e.Request.Latitude == "111111" &
                    e.Request.Longitude == "222222222"
                    select e;

            q = q.Where(e => e.Request.Within == "10m");
            var orderquery = q.OrderBy(e => e.Request.ByDate);

            Assert.IsTrue(orderquery.ToString() == "location.within=10m&location.latitude=111111&location.longitude=222222222&sort_by=ByDate");
        }

        [TestMethod]
        public void CanExtendQueryWithOrderByDifferentOrder()
        {
            EventbriteContext context = new EventbriteContext("xxxxxxx");

            var q = from e in context.Query
                    where e.Request.Latitude == "111111" &
                    e.Request.Longitude == "222222222"
                    select e;
            var orderquery = q.OrderBy(e => e.Request.ByDate);
            q = orderquery.Where(e => e.Request.Within == "10m");


            Assert.IsTrue(q.ToString() == "location.within=10m&location.latitude=111111&location.longitude=222222222&sort_by=ByDate");
        }

        [TestMethod]
        [ExpectedException(typeof(NotSupportedException))]
        public void CanNotExtendOrderBy()
        {
            EventbriteContext context = new EventbriteContext("xxxxxxx");

            var oq = from e in context.Query
                     orderby e.Request.ByDate
                     select e;

            var q = oq.Where(e => e.Request.Within == "10m");
            q = q.Where(e => e.Request.Latitude == "111111" && e.Request.Longitude == "222222222");

            var result = q.ToString();

            Assert.Fail();
        }

        // used by CanCaptureValues
        long TestOrganizer() => 1111;

        // used by CanCaptureValues
        class TestParams
        {
            public string Country() => "US";
            public string City => "Indianapolis";

            public Categories Categories => Categories.Business;

            public static DateKeywords DateModified => DateKeywords.Today;
        }

        [TestMethod]
        public void CanCaptureValues()
        {
            EventbriteContext context = new EventbriteContext("xxxxxxx");

            var region = "IN";
            TestParams tp = new TestParams();
            var q = from e in context.Query
                    where e.Request.Region == region &
                    e.Request.Organizer == TestOrganizer() &
                    e.Request.Country == tp.Country() &
                    e.Request.City == tp.City &
                    e.Request.Categories == tp.Categories &
                    e.Request.DateKeyword == (DateKeywords)6 &
                    e.Request.DateModifiedKeyword == TestParams.DateModified
                    select e;

            Assert.IsTrue(q.ToString() == "organizer.id=1111&categories=101&start_date.keyword=next_month&date_modified.keyword=today&location.address=Indianapolis,IN,US");

        }

        [TestMethod]
        public void CanUseSelectBeforeWhere()
        {
            EventbriteContext context = new EventbriteContext("xxxxxxx");

            var q = context.Query.Where(e => e.Request.City == "rrrr").Select(e => new { Name = e.Name.Text, Country = e.Venue.Address.Country });
            q = q.Where(o => o.Country == "xxxx");

            var qq = context.Query.Select(e => new { Name = e.Name.Text, Country = e.Venue.Address.Country }).Where(o => o.Country == "US");

            Assert.IsTrue(q.ToString() == "location.address=rrrr,xxxx");
            Assert.IsTrue(qq.ToString() == "location.address=US");
        }

    }
}
