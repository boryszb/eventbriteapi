﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EventbriteClient.Model;
using Eventbrite.LinqProvider;
using System.Linq.Expressions;

namespace LinqProviderTests
{
    /// <summary>
    /// Summary description for QueryTranslatorTests
    /// </summary>
    [TestClass]
    public class QueryTranslatorTests
    {
        public QueryTranslatorTests()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void CanTranslateCityParam()
        {
            Expression<Func<Event, bool>> predicate = e => e.Request.City == "London";
            QueryExpression query = new QueryExpression(null, predicate, null, null);
            QueryTranslator translator = new QueryTranslator();

            var result = translator.Translate(query);

            Assert.IsTrue(result == "location.address=London");

        }

        [TestMethod]
        public void CanTranslateCountryParam()
        {
            Expression<Func<Event, bool>> predicate = e => e.Request.Country == "US";
            QueryExpression query = new QueryExpression(null, predicate, null, null);
            QueryTranslator translator = new QueryTranslator();

            var result = translator.Translate(query);

            Assert.IsTrue(result == "location.address=US");
        }

        [TestMethod]
        public void CanTranslateRegionParam()
        {
            Expression<Func<Event, bool>> predicate = e => e.Request.Region == "IN";
            QueryExpression query = new QueryExpression(null, predicate, null, null);
            QueryTranslator translator = new QueryTranslator();

            var result = translator.Translate(query);

            Assert.IsTrue(result == "location.address=IN");
        }

        [TestMethod]
        public void CanTranslateRegionAndCountryParam()
        {
            Expression<Func<Event, bool>> predicate = e => e.Request.Region == "IN" & e.Request.Country == "PL";
            QueryExpression query = new QueryExpression(null, predicate, null, null);
            QueryTranslator translator = new QueryTranslator();

            var result = translator.Translate(query);
            
            Assert.IsTrue(result == "location.address=IN,PL");

        }

        [TestMethod]
        public void CanTranslateRegionAndCountryWithDiffOrderParam()
        {
            Expression<Func<Event, bool>> predicate = e => e.Request.Country == "PL" & e.Request.Region == "IN";
            QueryExpression query = new QueryExpression(null, predicate, null, null);
            QueryTranslator translator = new QueryTranslator();

            var result = translator.Translate(query);

            Assert.IsTrue(result == "location.address=IN,PL");
        }

        [TestMethod]
        public void CanTranslateRegionCountryCityParam()
        {
            Expression<Func<Event, bool>> predicate = e => e.Request.Country == "PL" & e.Request.Region == "IN" & e.Request.City == "Denver";
            QueryExpression query = new QueryExpression(null, predicate, null, null);
            QueryTranslator translator = new QueryTranslator();

            var result = translator.Translate(query);
           
            Assert.IsTrue(result == "location.address=Denver,IN,PL");
        }

        [TestMethod]
        public void CanTranslateRegionCountryCityWithDiffOrderParam()
        {
            QueryTranslator translator = new QueryTranslator();

            Expression<Func<Event, bool>> predicate1 = e => e.Request.Country == "PL" & e.Request.Region == "IN" & e.Request.City == "Denver";
            QueryExpression query1 = new QueryExpression(null, predicate1, null, null);

            Expression<Func<Event, bool>> predicate2 = e => e.Request.Country == "PL" & e.Request.City == "Denver" & e.Request.Region == "IN";
            QueryExpression query2 = new QueryExpression(null, predicate2, null, null);

            Expression<Func<Event, bool>> predicate3 = e => e.Request.City == "Denver" & e.Request.Region == "IN" & e.Request.Country == "PL";
            QueryExpression query3 = new QueryExpression(null, predicate3, null, null);

            // act & assert
            Assert.IsTrue(translator.Translate(query1) == "location.address=Denver,IN,PL");
            Assert.IsTrue(translator.Translate(query2) == "location.address=Denver,IN,PL");
            Assert.IsTrue(translator.Translate(query3) == "location.address=Denver,IN,PL");
        }

        [TestMethod]
        public void CanTranslateCategoriesParam()
        {
            QueryTranslator translator = new QueryTranslator();

            Expression<Func<Event, bool>> predicate1 = e => e.Request.Categories == Categories.Business;
            QueryExpression query1 = new QueryExpression(null, predicate1, null, null);

            Expression<Func<Event, bool>> predicate2 = e => e.Request.Categories == Categories.Arts;
            QueryExpression query2 = new QueryExpression(null, predicate2, null, null);

            Expression<Func<Event, bool>> predicate3 = e => e.Request.Categories == Categories.ScienceTech;
            QueryExpression query3 = new QueryExpression(null, predicate3, null, null);

            // act & assert
            Assert.IsTrue(translator.Translate(query1) == "categories=101");
            Assert.IsTrue(translator.Translate(query2) == "categories=105");
            Assert.IsTrue(translator.Translate(query3) == "categories=102");
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException), "Request url can have only one categories param")]
        public void OnlyOneParamForQuery()
        {

            QueryTranslator translator = new QueryTranslator();

            Expression<Func<Event, bool>> predicate = e => e.Request.Categories == Categories.Business & e.Request.Categories == Categories.Arts;
            QueryExpression query = new QueryExpression(null, predicate, null, null);

            translator.Translate(query);
        }

        [TestMethod]
        public void CanTranslateFormatsParam()
        {
            QueryTranslator translator = new QueryTranslator();

            Expression<Func<Event, bool>> predicate1 = e => e.Request.Formats == Formats.Appearance;
            QueryExpression query1 = new QueryExpression(null, predicate1, null, null);

            Expression<Func<Event, bool>> predicate2 = e => e.Request.Formats == Formats.Conference;
            QueryExpression query2 = new QueryExpression(null, predicate2, null, null);

            Expression<Func<Event, bool>> predicate3 = e => e.Request.Formats == Formats.Seminar;
            QueryExpression query3 = new QueryExpression(null, predicate3, null, null);

            Assert.IsTrue(translator.Translate(query1) == "formats=Appearance");
            Assert.IsTrue(translator.Translate(query2) == "formats=Conference");
            Assert.IsTrue(translator.Translate(query3) == "formats=Seminar");
        }

        [TestMethod]
        public void CanTranslateDateParams()
        {
            QueryTranslator translator = new QueryTranslator();

            Expression<Func<Event, bool>> predicate = e => e.Request.DateKeyword == DateKeywords.NextMonth & e.Request.DateModifiedKeyword == DateKeywords.Today;
            QueryExpression query = new QueryExpression(null, predicate, null, null);

            Assert.IsTrue(translator.Translate(query) == "start_date.keyword=next_month&date_modified.keyword=today");
        }

        [TestMethod]
        public void CanTranslateGeolocationParameters()
        {
            QueryTranslator translator = new QueryTranslator();

            Expression<Func<Event, bool>> predicate = e => e.Request.Latitude == "11111111" &
                                                            e.Request.Longitude == "222222222" &
                                                            e.Request.Within == "15km";
            QueryExpression query = new QueryExpression(null, predicate, null, null);

            Assert.IsTrue(translator.Translate(query) == "location.latitude=11111111&location.longitude=222222222&location.within=15km");
        }

        [TestMethod]
        public void CanTranslateOrganizerId()
        {
            QueryTranslator translator = new QueryTranslator();

            Expression<Func<Event, bool>> predicate = e => e.Request.Organizer == 1111111;
            QueryExpression query = new QueryExpression(null, predicate, null, null);

            Assert.IsTrue(translator.Translate(query) == "organizer.id=1111111");
        }

        [TestMethod]
        [ExpectedException(typeof(NotSupportedException), "The binary operator AndAlso is not supported")]
        public void CanUseOnlyAndOperator()
        {
            QueryTranslator translator = new QueryTranslator();

            Expression<Func<Event, bool>> predicate = e => e.Request.Organizer == 1111111 && e.Request.Categories == Categories.Arts;
            QueryExpression query = new QueryExpression(null, predicate, null, null);

            translator.Translate(query);
        }

        [TestMethod]
        public void CanTranslateAddressWithOtherParams()
        {
            QueryTranslator translator = new QueryTranslator();

            Expression<Func<Event, bool>> predicate = e => e.Request.Organizer == 1111111 &
                                                e.Request.City == "Denver" &
                                                e.Request.Categories == Categories.Arts &
                                                e.Request.Country == "US" &
                                                e.Request.DateKeyword == DateKeywords.Tomorrow &
                                                e.Request.Formats == Formats.Class &
                                                e.Request.Region == "CO" &
                                                e.Request.Within == "5m";

            QueryExpression query = new QueryExpression(null, predicate, null, null);
            const string QUERYSTRING = "organizer.id=1111111&categories=105&start_date.keyword=tomorrow&formats=Class&location.within=5m&location.address=Denver,CO,US";

            Assert.IsTrue(translator.Translate(query) == QUERYSTRING);
        }

        public void CanTranslateAllParams()
        {
            QueryTranslator translator = new QueryTranslator();

            Expression<Func<Event, bool>> predicate = e => e.Request.Organizer == 1111111 &
                                                e.Request.City == "Denver" &
                                                e.Request.Categories == Categories.Arts &
                                                e.Request.Country == "US" &
                                                e.Request.DateKeyword == DateKeywords.Tomorrow &
                                                e.Request.Formats == Formats.Class &
                                                e.Request.Region == "CO" &
                                                e.Request.Within == "5m" &
                                                e.Request.Latitude == "33333333" &
                                                e.Request.Longitude == "4444444" &
                                                e.Request.DateModifiedKeyword == DateKeywords.NextWeek &
                                                e.Request.Keywords == "aaa,bbbbb";

            QueryExpression query = new QueryExpression(null, predicate, null, null);
            const string QUERYSTRING = "organizer.id=1111111&categories=105&start_date.keyword=tomorrow&formats=Class"
                + "&location.within=5m&location.latitude=33333333&location.longitude=4444444&date_modified.keyword=next_week&q=aaa,bbbbb&location.address=Denver,CO,US";

            Assert.IsTrue(translator.Translate(query) == QUERYSTRING);
        }

        [TestMethod]
        public void CanTranslateOrderBy()
        {
            QueryTranslator translator = new QueryTranslator();
            Expression<Func<Event, bool>> predicate = e => e.Request.Organizer == 1111111;
            var member = typeof(EventSearchRequest).GetProperty("ByDate");
            var order = new OrderExpression(OrderType.Ascending, member);
            QueryExpression query = new QueryExpression(null, predicate, null, order);

            Assert.IsTrue(translator.Translate(query) == "organizer.id=1111111&sort_by=ByDate");

        }

        [TestMethod]
        public void CanTranslateOrderByDescending()
        {
            QueryTranslator translator = new QueryTranslator();
            Expression<Func<Event, bool>> predicate = e => e.Request.Organizer == 1111111;
            var member = typeof(EventSearchRequest).GetProperty("ByDate");
            var order = new OrderExpression(OrderType.Descending, member);
            QueryExpression query = new QueryExpression(null, predicate, null, order);

            Assert.IsTrue(translator.Translate(query) == "organizer.id=1111111&sort_by=-ByDate");

        }

        [TestMethod]
        public void CanTranslateOrderExpression()
        {
            var member = typeof(EventSearchRequest).GetProperty("ByDate");
            var order = new OrderExpression(OrderType.Ascending, member);

            Assert.IsTrue(order.ToString() == "&sort_by=ByDate");

        }

        [TestMethod]
        public void CanTranslateOrderExpressionDescending()
        {
            var member = typeof(EventSearchRequest).GetProperty("ByDate");
            var order = new OrderExpression(OrderType.Descending, member);

            Assert.IsTrue(order.ToString() == "&sort_by=-ByDate");

        }
    }
}
