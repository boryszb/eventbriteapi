﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EventbriteClient.Model;
using Eventbrite.LinqProvider;
using System.Linq.Expressions;

namespace LinqProviderTests
{
    [TestClass]
    public class QueryExpressionTests
    {
        [TestMethod]
        public void CanCreateInstance()
        {
            var query = new QueryExpression(null, null, null, null);
            Assert.IsNotNull(query);
        }

        [TestMethod]
        public void InstanceIsQueryExpressionType()
        {
            var query = new QueryExpression(null, null, null, null);
            Assert.IsTrue(query.NodeType == (ExpressionType)EventBriteExpression.QueryExpressionType);
        }

        [TestMethod]
        public void CanCreateInstanceWithType()
        {
            var type = typeof(Event);
            var query = new QueryExpression(type, null, null, null);
            Assert.IsTrue(query.Type == type && query.Type.Name == type.Name);
        }

        [TestMethod]
        public void CanCreateInstanceWithQuery()
        {
            Expression<Func<Event, bool>> q = e => e.Request.City == "Denver";
            var query = new QueryExpression(null, q, null, null);
            Assert.IsTrue(query.Query == q);
        }

        [TestMethod]
        public void CanCreateInstanceWithProjector()
        {
            Expression<Func<Event, object>> p = e => new { City = e.Request.City };
            var query = new QueryExpression(null, null, p, null);
            Assert.IsTrue(query.Projector == p);
        }

        [TestMethod]
        public void CanCreateInstanceWithOrder()
        {
            var member = typeof(EventSearchRequest).GetProperty("ByDate");
            var o = new OrderExpression(OrderType.Ascending, member);
            var query = new QueryExpression(null, null, null, o);
            Assert.IsTrue(query.Order == o);
        }
    }
}
