﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using EventbriteClient.Model;
using Eventbrite.LinqProvider;

namespace LinqProviderTests
{
    [TestClass]
    public class QueryBinderTests
    {
        readonly IQueryable<Event> _fakeQuery = new List<Event>().AsQueryable<Event>();

        [TestMethod]
        public void CanCreateInstance()
        {
            var queryBinder = new QueryBinder();
            Assert.IsNotNull(queryBinder);
            Assert.IsInstanceOfType(queryBinder, typeof(QueryBinder));
        }

        [TestMethod]
        public void CanBindQueryExpressionWithEventBriteExpression()
        {
            var q = _fakeQuery.Where(e => e.Request.City == "xxxx");
            var queryBinder = new QueryBinder();

            var result = queryBinder.Bind(q.Expression);
            Assert.IsTrue(result.NodeType == (ExpressionType)EventBriteExpression.QueryExpressionType);
        }

        [TestMethod]
        public void CanBindQueryExpressionWithEventType()
        {
            var q = _fakeQuery.Where(e => e.Request.City == "xxxx");
            var queryBinder = new QueryBinder();

            var result = queryBinder.Bind(q.Expression);
            Assert.IsTrue(result.Type == typeof(Event));
        }

        [TestMethod]
        public void CanBindQueryExpressionWithLambdaExpression()
        {
            var q = _fakeQuery.Where(e => e.Request.City == "xxxx");
            var queryBinder = new QueryBinder();

            var result = queryBinder.Bind(q.Expression) as QueryExpression;
            Assert.IsTrue(result.Query.NodeType == ExpressionType.Lambda);
        }

        [TestMethod]
        public void CanBindQueryExpressionWithFuncFilter()
        {

            Expression<Func<Event, bool>> exp = e => e.Request.City == "xxxx";
            var q = _fakeQuery.Where(exp);
            var queryBinder = new QueryBinder();

            var result = queryBinder.Bind(q.Expression) as QueryExpression;
            var body = (result.Query as LambdaExpression).Body;
            Assert.IsTrue(body.ToString() == exp.Body.ToString());
        }

        [TestMethod]
        public void CanBindQueryExpressionWithManyWhereExpressions()
        {
            // target express
            const string targetExpression = "((e.Request.Country == \"UK\") And ((Convert(e.Request.Categories) == 105) And (e.Request.City == \"London\")))";

            // construct query
            var query = _fakeQuery.Where(e => e.Request.City == "London");
            query = query.Where(e => e.Request.Categories == Categories.Arts);
            query = query.Where(e => e.Request.Country == "UK");
            var queryBinder = new QueryBinder();

            // act and assert
            var result = queryBinder.Bind(query.Expression) as QueryExpression;
            var body = (result.Query as LambdaExpression).Body;
            Assert.IsTrue(body.ToString() == targetExpression);
        }

        [TestMethod]
        public void CanBindQueryExpressionWithOrderExpression()
        {
            var query = _fakeQuery.OrderBy(e => e.Request.ByDate);
            var queryBinder = new QueryBinder();

            var result = queryBinder.Bind(query.Expression) as QueryExpression;

           Assert.IsInstanceOfType(result.Order, typeof(OrderExpression));

        }

        [TestMethod]
        public void CanOrderByDateAscending()
        {
            var query = _fakeQuery.OrderBy(e => e.Request.ByDate);
            var queryBinder = new QueryBinder();

            var result = queryBinder.Bind(query.Expression) as QueryExpression;

            Assert.IsTrue(result.Order.OrderType == OrderType.Ascending);
            Assert.IsTrue(result.Order.Member.Name == "ByDate");
        }


        [TestMethod]
        public void CanOrderByDateDescending()
        {
            var query = _fakeQuery.OrderByDescending(e => e.Request.ByDate);
            var queryBinder = new QueryBinder();

            var result = queryBinder.Bind(query.Expression) as QueryExpression;

            Assert.IsTrue(result.Order.OrderType == OrderType.Descending);
            Assert.IsTrue(result.Order.Member.Name == "ByDate");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "not supported filed City")]
        public void CanOrderOnlyByDate()
        {
            var query = _fakeQuery.OrderBy(e => e.Request.City);
            var queryBinder = new QueryBinder();

            var result = queryBinder.Bind(query.Expression) as QueryExpression;

        }

        [TestMethod]
        public void OrderByCanBeBeforeWhere()
        {
            var query = _fakeQuery.OrderBy(e => e.Request.ByDate);
            var q = query.Where(e => e.Request.City == "xxxx");

            var queryBinder = new QueryBinder();
            var result = queryBinder.Bind(q.Expression) as QueryExpression;

            Assert.IsTrue(result.Order.OrderType == OrderType.Ascending && result.Order.Member.Name == "ByDate");
            Assert.IsTrue((result.Query as LambdaExpression).Body.ToString() == "(e.Request.City == \"xxxx\")");
        }

        [TestMethod]
        public void ProjectorCanBeBeforWhere()
        {
            var project = _fakeQuery.Select(e => new { Name = e.Name.Text, City = e.Venue.Address.City }).Where(a => a.City == "xxx");
            var queryBinder = new QueryBinder();

            var result = queryBinder.Bind(project.Expression) as QueryExpression;

            // query
            Assert.IsTrue((result.Query as LambdaExpression).Body.ToString() == "(a.City == \"xxx\")");

            // projector
            Assert.IsInstanceOfType(result.Projector, typeof(LambdaExpression));

        }

        [TestMethod]
        public void CanBindFullEventbriteQuery()
        {
            var query = _fakeQuery.Where(e => e.Request.City == "xxxx").OrderBy(e => e.Request.ByDate).Select(e => new { Url = e.Url });
            var queryBinder = new QueryBinder();

            var result = queryBinder.Bind(query.Expression) as QueryExpression;

            // query
            Assert.IsTrue((result.Query as LambdaExpression).Body.ToString() == "(e.Request.City == \"xxxx\")");

            // order
            Assert.IsInstanceOfType(result.Order, typeof(OrderExpression));

            // projector
            Assert.IsInstanceOfType(result.Projector, typeof(LambdaExpression));

        }
    }
}
